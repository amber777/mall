<?php /*a:2:{s:53:"C:\wamp64\www\other\wn\app\admin\view\index\home.html";i:1642748112;s:47:"C:\wamp64\www\other\wn\app\admin\view\main.html";i:1606706270;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="images/x-icon" href="<?php echo xn_cfg('base.logo'); ?>">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<style>
.store-total-container {
    font-size: 14px;
    margin-bottom: 5px;
    letter-spacing: 1px;
}

.store-total-container .store-total-icon {
    top: 45%;
    right: 8%;
    font-size: 65px;
    position: absolute;
    color: rgba(255, 255, 255, 1);
}

.store-total-container .store-total-item {
    color: #fff;
    line-height: 3em;
    padding: 8px 25px;
    position: relative;
}

.store-total-container .store-total-item > div:nth-child(2) {
    font-size: 42px;
    line-height: 42px;
    /*width:100%*/
}

</style>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">数据统计</div>
        <div class="layui-card-body">
            <div class="think-box-shadow store-total-container notselect">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-sm6 layui-col-md3">
                        <div class="store-total-item nowrap" style="background:#06b9a8">
                            <div>评价总量</div>
                            <div>56</div>
                            <div>用户留言评论总量</div>
                        </div>
                        <i class="store-total-icon layui-icon layui-icon-reply-fill"></i>
                    </div>
                    <div class="layui-col-sm6 layui-col-md3">
                        <div class="store-total-item nowrap" style="background:#3db9dc">
                            <div>订单总量</div>
                            <div>856</div>
                            <div>已付款订单总数量</div>
                        </div>
                        <i class="store-total-icon layui-icon layui-icon-cart"></i>
                    </div>
                    <div class="layui-col-sm6 layui-col-md3">
                        <div class="store-total-item nowrap" style="background:#f1b53d">
                            <div>用户总量</div>
                            <div>1,123</div>
                            <div>当前用户总数量</div>
                        </div>
                        <i class="store-total-icon layui-icon layui-icon-user"></i>
                    </div>
                    <div class="layui-col-sm6 layui-col-md3">
                        <div class="store-total-item nowrap" style="background:#ff5d48;">
                            <div>待处理</div>
                            <div>11</div>
                            <div>待处理信息</div>
                        </div>
                        <i class="store-total-icon layui-icon layui-icon-notice"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-row layui-col-space15">
        <div class="layui-col-md9">
            <div class="layui-card">
                <div class="layui-card-header">数据概览</div>
                <div class="layui-card-body layui-text">
                    <div id="my_chart" style="width: 100%;height: 400px;"></div>
                </div>
            </div>
        </div>
        <div class="layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">服务器信息</div>
                <div class="layui-card-body layui-text">
                    <table class="layui-table">
                        <?php if(is_array($server_info) || $server_info instanceof \think\Collection || $server_info instanceof \think\Paginator): if( count($server_info)==0 ) : echo "" ;else: foreach($server_info as $key=>$vo): if($k%2==1): ?><tr><?php endif; ?>
                        <td width="100"><?php echo htmlentities($key); ?>：</td>
                        <td><?php echo htmlentities($vo); ?></td>
                        <?php if($k%2==0): ?></tr><?php endif; ?>
                        <?php endforeach; endif; else: echo "" ;endif; if(count($caches)%2==1): ?><td>&nbsp;</td><td>&nbsp;</td></tr><?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-row ">
        <div class="layui-col-md9">
        </div>
    </div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<!--js渲染动画效果-->
<script src="/static/admin/js/echarts.js"></script>
<script>
    var myChart = echarts.init(document.getElementById('my_chart'));
    option = {
        title: {
            text: '堆叠区域图'
        },
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        legend: {
            data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '邮件营销',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [120, 132, 101, 134, 90, 230, 210]
            },
            {
                name: '联盟广告',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '视频广告',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '直接访问',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [320, 332, 301, 334, 390, 330, 320]
            },
            {
                name: '搜索引擎',
                type: 'line',
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                areaStyle: {},
                data: [820, 932, 901, 934, 1290, 1330, 1320]
            }
        ]
    };
    myChart.setOption(option);
</script>

</body>
</html>
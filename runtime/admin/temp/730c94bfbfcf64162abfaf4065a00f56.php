<?php /*a:3:{s:53:"C:\wamp64\www\other\wn\app\admin\view\sell\index.html";i:1642126657;s:47:"C:\wamp64\www\other\wn\app\admin\view\main.html";i:1606706270;s:53:"C:\wamp64\www\other\wn\app\admin\view\breadcrumb.html";i:1583051252;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="images/x-icon" href="<?php echo xn_cfg('base.logo'); ?>">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<?php if(!empty($breadcrumb)): ?>
<div class="layui-card layadmin-header">
    <div class="layui-breadcrumb" lay-filter="breadcrumb" style="visibility: visible;">
        <a href="<?php echo url('admin/index/home'); ?>">主页</a>
        <?php if(is_array($breadcrumb) || $breadcrumb instanceof \think\Collection || $breadcrumb instanceof \think\Paginator): if( count($breadcrumb)==0 ) : echo "" ;else: foreach($breadcrumb as $key=>$vo): ?>
        <a><cite><?php echo htmlentities($vo['title']); ?></cite></a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="layui-fluid">
    <div class="layui-card">
        <div class=" layui-card-header layuiadmin-card-header-auto">
            <form class="layui-form" method="get">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <a href="<?php echo url('add'); ?>"  data-width="600px" data-height="450px" class="layui-btn   xn_open" title="添加栏目">
                            <i class="layui-icon layui-icon-addition layuiadmin-button-btn"></i>
                        </a>
                    </div>
<!--                    <div></div> -->
                    <!--<div class="layui-inline">
                        <button class="layui-btn">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>
                    </div>-->
                </div>
            </form>
        </div>
        <style>
            .layui-table tr th{text-align: center}
        </style>
        <div class="layui-card-body">
            <table class="layui-table" style="text-align: center">
                <tr>
                    <th><b>ID</b></th>
                    <th><b>名字</b></th>
                    <th><b>跳转地址</b></th>
                    <th><b>创建时间</b></th>
                    <th><b>操作</b></th>
                </tr>
                <tbody>
                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo htmlentities($vo['id']); ?></td>
                    <td><?php echo htmlentities($vo['name']); ?></td>
                    <td><?php echo htmlentities($vo['url']); ?></td>
                    <td><?php echo htmlentities($vo['create_time']); ?></td>
                    <td>
                        <a href="<?php echo Url('delete',array('id'=>$vo['id'])); ?>" title="确定要删除吗？"
                           class="layui-btn layui-btn-danger layui-btn-xs xn_delete">
                            <i class="layui-icon layui-icon-delete"></i>删除
                        </a>
                        <a href="<?php echo url('edit',array('id'=>$vo['id'],'pid'=>$vo['pid'])); ?>"
                           data-width="600px" data-height="450px"
                           title="<?php echo htmlentities($vo['title']); ?>" class="layui-btn layui-btn-normal layui-btn-xs xn_open">
                            <i class="layui-icon layui-icon-edit"></i>修改
                        </a>
                    </td>
                </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pages"><?php echo $list; ?></div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        laydate.render({
            elem: '#start_date'
        });
        laydate.render({
            elem: '#end_date'
        });
    });
</script>

</body>
</html>
<?php /*a:2:{s:51:"C:\wamp64\www\other\wn\app\admin\view\news\add.html";i:1606901479;s:49:"C:\wamp64\www\other\wn\app\admin\view\iframe.html";i:1584595684;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-form" lay-filter="layuiadmin-app-form-list" id="layuiadmin-app-form-list">
            <form action="<?php echo request()->url(); ?>" method="post" class="xn_ajax" data-type="open">

                <div class="layui-form-item">
                    <label class="layui-form-label">文章标题</label>
                    <div class="layui-input-inline">
                        <input type="text" name="tittle" placeholder="文章标题" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['tittle']); ?>">
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">文章简介</label>
                    <div class="layui-input-block">
                        <input type="text" name="desc" placeholder="请简述文章概要" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['desc']); ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">文章封面</label>
                    <?php if($data['img'] != ''): ?>
                    <div class="layui-input-block ">
                        <input id="input_if_has_img" type="hidden" name="img" class="layui-input xn-images" value="<?php echo htmlentities($data['img']); ?>">
                        <img  id="if_has_img" src="<?php echo htmlentities($data['img']); ?>" width="150px" >
                        <span id="close_if_has_img" class="layui-icon layui-icon-close"></span>
                    </div>
                    <div class="layui-input-block"><span></span></div>
                    <?php else: ?>

                    <?php endif; ?>
                    <div class="layui-input-block ">
                        <?php echo pic_choice_one($data['img'],'img'); ?>
                    </div>
                </div>




                <div class="layui-form-item">
                    <label class="layui-form-label">文章类型</label>
                    <div class="layui-input-inline">
                        <select name="type">
                            <option value="">请选择文章类型</option>
                            <?php foreach($news_type as $key=>$vo): ?>
                            <option value="<?php echo htmlentities($key); ?>" <?php if($key == $data['type']): ?>selected="selected"<?php endif; ?>><?php echo htmlentities($vo); ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">文章内容</label>
                    <div class="layui-input-block">
                        <textarea id="content" name="content" placeholder="请输入文章内容~"><?php echo htmlentities((isset($data['content']) && ($data['content'] !== '')?$data['content']:"")); ?></textarea>
                    </div>
                </div>



                <div class="layui-form-item">
                    <label class="layui-form-label">文章来源</label>
                    <div class="layui-input-block">
                        <input type="text" name="from" placeholder=" 若没有请写原创" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['from']); ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">文章作者</label>
                    <div class="layui-input-inline">
                        <input type="text" name="author" placeholder="文章作者" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['author']); ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">排序赋值</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sort" placeholder="排序值越高越靠前,最大值999" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['sort']); ?>">
                    </div>
                </div>



                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn">确定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    //建立编辑器

    layui.use('layedit', function(){

        var layedit = layui.layedit;
        $ = layui.jquery;
        layedit.build('content', {
            tool: [
                'strong'        //加粗
                ,'italic'       //斜体
                ,'underline'    //下划线
                ,'del'          //删除线
                ,'|'            //分割线
                ,'left'         //左对齐
                ,'center'       //居中对齐
                ,'right'        //右对齐
                ,'link'         //超链接
                ,'unlink'       //清除链接
                ,'face'         //表情
                //,'image'        //插入图片
                // ,'help'         //帮助
            ],
            height:200,
        });
    });


    //前端处理图片删除
    $(document).ready(function(){
        var close_btn = $("#close_if_has_img");
        close_btn.mouseover(function(){
            close_btn.css("cursor","pointer");
        });
        close_btn.on("click",function () {
            //删除节点
            $("#if_has_img").remove();
            //删除input值
            $("#input_if_has_img").val("");
            //删除本身
            this.remove();
            this.parent().remove();
        })
    });


</script>

</body>
</html>
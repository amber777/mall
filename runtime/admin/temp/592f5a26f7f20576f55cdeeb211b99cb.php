<?php /*a:3:{s:73:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\config\upload.html";i:1583504006;s:64:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\main.html";i:1606118849;s:70:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\breadcrumb.html";i:1583051252;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<?php if(!empty($breadcrumb)): ?>
<div class="layui-card layadmin-header">
    <div class="layui-breadcrumb" lay-filter="breadcrumb" style="visibility: visible;">
        <a href="<?php echo url('admin/index/home'); ?>">主页</a>
        <?php if(is_array($breadcrumb) || $breadcrumb instanceof \think\Collection || $breadcrumb instanceof \think\Paginator): if( count($breadcrumb)==0 ) : echo "" ;else: foreach($breadcrumb as $key=>$vo): ?>
        <a><cite><?php echo htmlentities($vo['title']); ?></cite></a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body" pad15="">
                    <form action="<?php echo request()->url(); ?>" method="post" class="xn_ajax">
                        <div class="layui-form" wid100="" lay-filter="">
                            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                                <legend>文件存储方式</legend>
                            </fieldset>
                            <div class="layui-form-item">
                                <label class="layui-form-label">存储方式</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="storage" lay-filter="radio_storage" class="radio_storage" value="0" title="本地" <?php if($data['storage'] == 0): ?>checked=""<?php endif; ?>>
                                    <input type="radio" name="storage" lay-filter="radio_storage" class="radio_storage" value="1" title="阿里云OSS" <?php if($data['storage'] == 1): ?>checked=""<?php endif; ?>>
                                    <input type="radio" name="storage" lay-filter="radio_storage" class="radio_storage" value="2" title="七牛云" <?php if($data['storage'] == 2): ?>checked=""<?php endif; ?>>
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_1" <?php if($data['storage'] != 1): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">AccessKey</label>
                                <div class="layui-input-block">
                                    <input type="text" name="oss_ak" value="<?php echo htmlentities($data['oss_ak']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_1" <?php if($data['storage'] != 1): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">AccessKeySecret</label>
                                <div class="layui-input-block">
                                    <input type="text" name="oss_sk" value="<?php echo htmlentities($data['oss_sk']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_1" <?php if($data['storage'] != 1): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">Endpoint</label>
                                <div class="layui-input-block">
                                    <input type="text" name="oss_endpoint" value="<?php echo htmlentities($data['oss_endpoint']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_1" <?php if($data['storage'] != 1): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">Bucket</label>
                                <div class="layui-input-block">
                                    <input type="text" name="oss_bucket" value="<?php echo htmlentities($data['oss_bucket']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_2" <?php if($data['storage'] != 2): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">AccessKey</label>
                                <div class="layui-input-block">
                                    <input type="text" name="qiniu_ak" value="<?php echo htmlentities($data['qiniu_ak']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_2" <?php if($data['storage'] != 2): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">AccessKeySecret</label>
                                <div class="layui-input-block">
                                    <input type="text" name="qiniu_sk" value="<?php echo htmlentities($data['qiniu_sk']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_2" <?php if($data['storage'] != 2): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">域名</label>
                                <div class="layui-input-block">
                                    <input type="text" name="qiniu_domain" value="<?php echo htmlentities($data['qiniu_domain']); ?>" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item storage_setting storage_2" <?php if($data['storage'] != 2): ?>style="display: none"<?php endif; ?>>
                                <label class="layui-form-label">Bucket</label>
                                <div class="layui-input-block">
                                    <input type="text" name="qiniu_bucket" value="<?php echo htmlentities($data['qiniu_bucket']); ?>" class="layui-input">
                                </div>
                            </div>
                            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                                <legend>图片处理</legend>
                            </fieldset>
                            <!--<div class="layui-form-item">
                                <label class="layui-form-label">生成缩略图</label>
                                <div class="layui-input-block">
                                    <input type="text" name="thumb" value="<?php echo htmlentities($data['thumb']); ?>" class="layui-input" placeholder="例如：200,200 多个缩略图用“|”号隔开，不生成请留空">
                                </div>
                            </div>
                            <hr>-->
                            <div class="layui-form-item">
                                <label class="layui-form-label">图片水印</label>
                                <div class="layui-input-block">
                                    <input type="checkbox" name="is_water" value="1" <?php if($data['is_water'] == 1): ?> checked<?php endif; ?> lay-skin="switch" lay-text="开启|关闭">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">水印文件路径</label>
                                <div class="layui-input-block">
                                    <input type="text" name="water_img" value="<?php echo htmlentities($data['water_img']); ?>" class="layui-input">
                                    <div class="layui-form-mid layui-word-aux">不能是网络图片</div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">水印位置</label>
                                <div class="layui-input-inline">
                                    <select name="water_locate">
                                        <option value="1" <?php if($data['water_locate'] == '1'): ?>selected<?php endif; ?>>左上角</option>
                                        <option value="2" <?php if($data['water_locate'] == '2'): ?>selected<?php endif; ?>>上居中</option>
                                        <option value="3" <?php if($data['water_locate'] == '3'): ?>selected<?php endif; ?>>右上角</option>
                                        <option value="4" <?php if($data['water_locate'] == '4'): ?>selected<?php endif; ?>>左居中</option>
                                        <option value="5" <?php if($data['water_locate'] == '5'): ?>selected<?php endif; ?>>居中</option>
                                        <option value="6" <?php if($data['water_locate'] == '6'): ?>selected<?php endif; ?>>右居中</option>
                                        <option value="7" <?php if($data['water_locate'] == '7'): ?>selected<?php endif; ?>>左下角</option>
                                        <option value="8" <?php if($data['water_locate'] == '8'): ?>selected<?php endif; ?>>下居中</option>
                                        <option value="9" <?php if($data['water_locate'] == '9'): ?>selected<?php endif; ?>>右下角</option>
                                    </select>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">透明度</label>
                                <div class="layui-input-inline">
                                    <input type="text" name="water_alpha" value="<?php echo htmlentities($data['water_alpha']); ?>" autocomplete="off" class="layui-input" placeholder="">
                                </div>
                                <div class="layui-form-mid layui-word-aux">0~100，数字越小，透明度越高</div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="set_website">确认保存</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    layui.use(['form','jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        //点击勾选
        form.on('radio(radio_storage)', function (data) {
            var value = $(this).val();
            $('.storage_setting').hide();
            $('.storage_' + value).show();
            form.render('radio');
        });
    });
</script>

</body>
</html>

<!--    基础main导入 -->
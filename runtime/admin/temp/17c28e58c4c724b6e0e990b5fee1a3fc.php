<?php /*a:3:{s:53:"C:\wamp64\www\other\wn\app\admin\view\news\index.html";i:1606902210;s:47:"C:\wamp64\www\other\wn\app\admin\view\main.html";i:1606706270;s:53:"C:\wamp64\www\other\wn\app\admin\view\breadcrumb.html";i:1583051252;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="images/x-icon" href="<?php echo xn_cfg('base.logo'); ?>">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<?php if(!empty($breadcrumb)): ?>
<div class="layui-card layadmin-header">
    <div class="layui-breadcrumb" lay-filter="breadcrumb" style="visibility: visible;">
        <a href="<?php echo url('admin/index/home'); ?>">主页</a>
        <?php if(is_array($breadcrumb) || $breadcrumb instanceof \think\Collection || $breadcrumb instanceof \think\Paginator): if( count($breadcrumb)==0 ) : echo "" ;else: foreach($breadcrumb as $key=>$vo): ?>
        <a><cite><?php echo htmlentities($vo['title']); ?></cite></a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="layui-fluid">
    <div class="layui-card">
        <div class=" layui-card-header layuiadmin-card-header-auto">
            <form class="layui-form" method="get">




                <div class="layui-form-item">
                    <div class="layui-inline">
                        <a href="<?php echo url('add'); ?>"  data-width="1000px" data-height="800px" class="layui-btn   xn_open" title="添加文章">
                            <i class="layui-icon layui-icon-addition layuiadmin-button-btn"></i>
                        </a>
                    </div>


                    <div class="layui-inline">
                        <label class="layui-form-label">文章类型</label>
                        <div class="layui-input-inline">
                            <select name="type">
                                <!--  ———————— -->
                                <option value="">——未选择——</option>
                                <?php foreach($news_type as $key=>$vo): ?>
                                <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                    <div class="layui-inline">
                        <label class="layui-form-label">文章标题</label>
                        <div class="layui-input-inline">
                            <input type="text" name="tittle"  class="layui-input" >
                        </div>
                    </div>

                    <!---->
                    <div class="layui-inline">
                        <button class="layui-btn">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"  title="搜索文章"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <style>
            .layui-table tr th{text-align: center}
        </style>
        <div class="layui-card-body">
            <table class="layui-table" style="text-align: center">
                <tr>
                    <th><b>文章ID</b></th>
                    <th><b>文章标题</b></th>
                    <th><b>文章封面</b></th>
                    <th><b>文章类型</b></th>
                    <th><b>文章简介</b></th>
                    <th><b>文章来源</b></th>
                    <th><b>文章状态</b></th>
                    <th><b>浏览量</b></th>
                    <th><b>创建时间</b></th>
                    <th><b>操作</b></th>
                </tr>

                <tbody>
                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo htmlentities($vo['id']); ?></td>
                    <!--  输出标题 -->
                    <td><?php echo htmlentities(mb_substr($vo['tittle'],0,12)); if(strlen($vo->tittle) > '30'): ?>......<?php endif; ?></td>
                    <?php if($vo['img'] != ''): ?>
                    <td><a href="javascript:;" onClick="showImg(this)" data-href="<?php echo htmlentities($vo['img']); ?>" ><img src="<?php echo htmlentities($vo['img']); ?>" width="80"></a></td>
                    <?php else: ?>
                    <td>无</td>
                    <?php endif; ?>
                    <td><?php echo htmlentities($news_type[$vo['type']]); ?></td>
                    <td><?php echo htmlentities(mb_substr($vo['desc'],0,20)); if(strlen($vo->desc) > '20'): ?>......<?php endif; ?></td>
                    <td><?php echo htmlentities(mb_substr($vo['from'],0,20)); if(strlen($vo->from) > '40'): ?>......<?php endif; ?></td>
                    <td class="layui-form">
                        <input type="checkbox" lay-verify="required" lay-filter="status" name="status" data-id="<?php echo htmlentities($vo['id']); ?>"
                               lay-skin="switch" lay-text="正常|禁用" value="1" <?php if($vo['status'] == 1): ?>checked<?php endif; ?> >
                    </td>
                    <td><?php echo htmlentities($vo['view_num']); ?></td>
                    <td><?php echo htmlentities($vo['create_time']); ?></td>
                    <td>
                        <a href="<?php echo Url('delete',array('id'=>$vo['id'])); ?>" title="确定要删除文章 <?php echo htmlentities($vo['tittle']); ?> 吗？"
                           class="layui-btn layui-btn-danger layui-btn-xs xn_delete">
                            <i class="layui-icon layui-icon-delete"></i>删除
                        </a>

                        <a href="<?php echo url('edit',array('id'=>$vo['id'])); ?>"
                           data-width="1000px" data-height="800px"
                           title="<?php echo htmlentities($vo['title']); ?>" class="layui-btn layui-btn-normal layui-btn-xs xn_open">
                            <i class="layui-icon layui-icon-edit"></i>修改
                        </a>
                    </td>
                </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pages"><?php echo $list; ?></div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>



<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        laydate.render({
            elem: '#start_date'
        });
        laydate.render({
            elem: '#end_date'
        });
    });


    layui.form.on('switch(status)', function(data){
        //layer.tips('开关checked：'+ (this.checked ? 'true' : 'false'), data.othis)
        var val = this.checked ? 1 : 0;
        var id = $(this).attr('data-id');
        var url = "<?php echo url('set_status'); ?>";
        $.post(url,{id:id,status:val},function (res) {
            console.log(res);
            layer.tips(res.msg, data.othis);
        },'json')
    });

    function showImg(obj){
        var url = $(obj).data('href');
        var thisimg = $(obj).siblings().find('img')
        var img = "<img src='" + url + "'  />";
        var setting = {
            type: 1,
            title: false,
            closeBtn: 0,
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            shade: 0.6, //遮罩透明度
            content: img
        }

        var windowH = $(window).height();
        var windowW = $(window).width();

        getImageWidth(url,function(w,h){
            //console.log("win:"+windowH+","+windowW);
            //console.log("img:"+h+","+w);
            // 调整图片大小
            if(w>windowW || h>windowH){
                if(w>windowW && h>windowH){
                    w = thisimg .width()*3;
                    h = thisimg .height()*3;
                    setting.area = [w+"px",h+"px"];
                }else if(w>windowW){
                    setting.area = [windowW+"px",windowW*0.5/w*h+"px"];
                }else{
                    setting.area = [w+"px",windowH+"px"];
                }
            }else{
                setting.area = [w+"px",h+"px"];
            }
            // 设置layer
            layer.open(setting);
        });
    }

    function getImageWidth(url,callback){
        var img = new Image();
        img.src = url;

        // 如果图片被缓存，则直接返回缓存数据
        if(img.complete){
            callback(img.width, img.height);
        }else{
            // 完全加载完毕的事件
            img.onload = function(){
                callback(img.width, img.height);
            }
        }


    }

</script>

</body>
</html>
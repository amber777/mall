<?php /*a:2:{s:77:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\upload_files\form.html";i:1606183712;s:66:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\iframe.html";i:1606120174;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-form" lay-filter="layuiadmin-app-form-list" id="layuiadmin-app-form-list" style="padding: 20px 30px 0 0;">
    <form action="<?php echo request()->url(); ?>" method="post" class="xn_ajax" data-type="open">

        <!--<div class="layui-form-item">
            <label class="layui-form-label">账号</label>
            <div class="layui-input-inline">
                <input type="text" name="username" lay-verify="required" placeholder="" autocomplete="off" class="layui-input" value="<?php echo htmlentities($user_data['username']); ?>">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">Logo</label>
            <div class="layui-input-block">
                <?php echo xn_upload_one($data['logo'],'logo',0); ?>
            </div>
        </div>
        -->

        <div class="layui-form-item">
            <div class="layui-input-block">
                <?php echo xn_upload_multi($data['logo'],'logo',0); ?>
            </div>
        </div>

    </form>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

</body>
</html>
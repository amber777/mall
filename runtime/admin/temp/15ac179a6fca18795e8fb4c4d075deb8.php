<?php /*a:2:{s:64:"C:\wamp64\www\other\wn\app\admin\view\auth_group\group_rule.html";i:1642752968;s:49:"C:\wamp64\www\other\wn\app\admin\view\iframe.html";i:1584595684;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-fluid">
    <form action="" method="post" class="xn_ajax layui-form">
        <input type="hidden" name="id" value="<?php echo htmlentities($group_data['id']); ?>">
        <table class="layui-table" style="margin-top: 0;">
            <?php if(is_array($rule_data) || $rule_data instanceof \think\Collection || $rule_data instanceof \think\Paginator): if( count($rule_data)==0 ) : echo "" ;else: foreach($rule_data as $key=>$v): ?>
            <notempty name="v['_data']">
                <!--<tr class="b-group">
                    <th>
                        <input type="checkbox" name="rule_ids[]" value="<?php echo htmlentities($v['id']); ?>" <?php if(in_array($v['id'],$group_data['rules'])): ?> checked="checked"<?php endif; ?> lay-skin="primary" title="<?php echo htmlentities($v['title']); ?>" lay-filter="allChoose">
                    </th>
                    <td></td>
                </tr>
                <else />-->
                <tr class="b-group">
                    <th>
                       <input type="checkbox" name="rule_ids[]" value="<?php echo htmlentities($v['id']); ?>" <?php if(in_array($v['id'],$group_data['rules'])): ?> checked="checked"<?php endif; ?> lay-skin="primary" title="<?php echo htmlentities($v['title']); ?>" lay-filter="allChoose">
                    </th>
                    <td class="b-child">
                        <?php if(is_array($v['_data']) || $v['_data'] instanceof \think\Collection || $v['_data'] instanceof \think\Paginator): if( count($v['_data'])==0 ) : echo "" ;else: foreach($v['_data'] as $key=>$n): ?>
                        <table class="layui-table">
                            <tr class="b-group">
                                <th width="120">
                                    <input type="checkbox" name="rule_ids[]" value="<?php echo htmlentities($n['id']); ?>" <?php if(in_array($n['id'],$group_data['rules'])): ?> checked="checked"<?php endif; ?> lay-skin="primary" title="<?php echo htmlentities($n['title']); ?>" lay-filter="allChoose">
                                </th>
                                <td>
                                    <?php if(!(empty($n['_data']) || (($n['_data'] instanceof \think\Collection || $n['_data'] instanceof \think\Paginator ) && $n['_data']->isEmpty()))): if(is_array($n['_data']) || $n['_data'] instanceof \think\Collection || $n['_data'] instanceof \think\Paginator): $i = 0; $__LIST__ = $n['_data'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$c): $mod = ($i % 2 );++$i;?>
                                        &emsp;<input type="checkbox" name="rule_ids[]" value="<?php echo htmlentities($c['id']); ?>" <?php if(in_array($c['id'],$group_data['rules'])): ?> checked="checked"<?php endif; ?> lay-skin="primary" title="<?php echo htmlentities($c['title']); ?>">
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </td>
                </tr>
            </notempty>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <tr>
                <th></th>
                <td>
                    <input class="layui-btn" type="submit" value="提交">
                </td>
            </tr>
        </table>
    </form>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    layui.use(['form','jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        //点击全选, 勾选
        form.on('checkbox(allChoose)', function (data) {
            var child = $(this).parents('.b-group').eq(0).find("input[type='checkbox']");
            child.each(function (index, item) {
                item.checked = data.elem.checked;
            });
            form.render('checkbox');
        });
    });
</script>

</body>
</html>
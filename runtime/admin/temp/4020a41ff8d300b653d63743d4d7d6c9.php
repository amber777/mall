<?php /*a:2:{s:53:"C:\wamp64\www\other\wn\app\admin\view\column\add.html";i:1606459129;s:49:"C:\wamp64\www\other\wn\app\admin\view\iframe.html";i:1584595684;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-form" lay-filter="layuiadmin-app-form-list" id="layuiadmin-app-form-list">
            <form action="<?php echo request()->url(); ?>" method="post" class="xn_ajax" data-type="open">
                <div class="layui-form-item">
                    <label class="layui-form-label">栏目名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="name" placeholder="栏目名称" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['name']); ?>">
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">栏目从属</label>
                    <div class="layui-input-inline">
                        <select name="pid" lay-verify="required">
                            <option value="0">顶级分类</option>
                            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                            <option value="<?php echo htmlentities($vo['id']); ?>" <?php if($pid == $vo['id']): ?>selected<?php endif; ?> ><?php echo htmlentities($vo['name']); ?></option>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">跳转地址</label>
                    <div class="layui-input-inline">
                        <input type="text" name="url" placeholder="跳转地址" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['url']); ?>">
                    </div>
                </div>


                <div class="layui-form-item">
                    <label class="layui-form-label">跳转类型</label>
                    <div class="layui-input-inline">

                        <input type="radio" name="type" value="2" title="当前页面" checked>
                        <input type="radio" name="type" value="1" title="打开新网页" >

                    </div>
                </div>



                <div class="layui-form-item">
                    <label class="layui-form-label">排序赋值</label>
                    <div class="layui-input-inline">
                        <input type="text" name="sort" placeholder="排序值越高越靠前" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['sort']); ?>">
                    </div>
                </div>



                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn">确定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

</body>
</html>
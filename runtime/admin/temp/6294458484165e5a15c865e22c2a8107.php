<?php /*a:3:{s:70:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\auth\index.html";i:1583247723;s:64:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\main.html";i:1606118849;s:70:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\breadcrumb.html";i:1583051252;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
<style>
    .layui-form-switch{margin-top: 0;}
</style>

</head>
<body>

<?php if(!empty($breadcrumb)): ?>
<div class="layui-card layadmin-header">
    <div class="layui-breadcrumb" lay-filter="breadcrumb" style="visibility: visible;">
        <a href="<?php echo url('admin/index/home'); ?>">主页</a>
        <?php if(is_array($breadcrumb) || $breadcrumb instanceof \think\Collection || $breadcrumb instanceof \think\Paginator): if( count($breadcrumb)==0 ) : echo "" ;else: foreach($breadcrumb as $key=>$vo): ?>
        <a><cite><?php echo htmlentities($vo['title']); ?></cite></a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="layui-fluid">
    <form action="<?php echo url('sort'); ?>" method="post">
    <table class="layui-table" style="margin-top: 0;">
        <tr>
            <th style="width: 45px;"><b>排序</b></th>
            <th><b>权限名</b></th>
            <th><b>权限</b></th>
            <th><b>菜单显示</b></th>
            <th style="padding: 0 15px;line-height: 40px;"><b>操作</b> <span style="float: right"><a href="<?php echo url('add',array('pid'=>$vo['id'])); ?>" data-width="600px" data-height="450px" class="layui-btn layui-btn-sm xn_open" title="添加权限">添加权限</a></span> </th>
        </tr>

        <tbody>
        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
        <tr>
            <td><input type="text" name="<?php echo htmlentities($vo['id']); ?>" placeholder="排序" autocomplete="off" class="layui-input" value="<?php echo htmlentities($vo['sort']); ?>" style="width: 45px;height:24px;line-height:24px;color: #999;"></td>
            <td><?php echo htmlentities($vo['_name']); ?></td>
            <td><?php echo htmlentities($vo['name']); ?></td>
            <td class="layui-form">
                <input type="checkbox" lay-verify="required" lay-filter="is_menu" name="is_menu" data-id="<?php echo htmlentities($vo['id']); ?>"
                       lay-skin="switch" lay-text="显示|隐藏" value="1" <?php if($vo['is_menu'] == 1): ?>checked<?php endif; ?> >
            </td>
            <td>
                <a href="<?php echo url('add',array('pid'=>$vo['id'])); ?>" data-width="600px" data-height="450px" class="layui-btn layui-btn-xs xn_open" title="添加子权限">
                    <i class="layui-icon layui-icon-addition"></i>添加子权限
                </a>
                <a href="<?php echo url('edit',array('id'=>$vo['id'])); ?>" data-width="600px" data-height="450px" title="<?php echo htmlentities($vo['title']); ?>" class="layui-btn layui-btn-normal layui-btn-xs xn_open">
                    <i class="layui-icon layui-icon-edit"></i>修改
                </a>
                <a href="<?php echo Url('auth/delete',array('id'=>$vo['id'])); ?>" title="确认要删除【<?php echo htmlentities($vo['title']); ?>】吗？" class="layui-btn layui-btn-danger layui-btn-xs xn_delete">
                    <i class="layui-icon layui-icon-delete"></i>删除
                </a>
            </td>
        </tr>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        <tr>
            <td colspan="5">
                <button type="submit" class="layui-btn">排序</button>
            </td>
        </tr>
        </tbody>
    </table>
    </form>
</div>

<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    layui.form.on('switch(is_menu)', function(data){
        //layer.tips('开关checked：'+ (this.checked ? 'true' : 'false'), data.othis)
        var val = this.checked ? 1 : 0;
        var id = $(this).attr('data-id');
        var url = "<?php echo url('edit'); ?>";
        $.post(url,{id:id,is_menu:val},function (res) {
            console.log(res);
            layer.tips(res.msg, data.othis);
        },'json')
    });
</script>

</body>
</html>

<!--    基础main导入 -->
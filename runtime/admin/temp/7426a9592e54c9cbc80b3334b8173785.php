<?php /*a:2:{s:79:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\upload_files\select.html";i:1606117381;s:66:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\iframe.html";i:1606120174;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
<style>
    #select-image-list{}
    #select-image-list .layui-card{position: relative;width: 180px;height: 180px;overflow: hidden;cursor: pointer;}
    #select-image-list img {width: 180px;position: absolute;}
    #select-image-list .layui-card.active span{position: absolute;width: 100%;height: 100%;background: rgba(0,0,0,0.5);text-align: center;padding-top: 60px;box-sizing: border-box;}
    #select-image-list .layui-card.active span i{font-size: 60px;color: #00ffa1;}
</style>

    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-fluid" id="LAY-component-grid-list">
    <div class="layui-row layui-col-space10" id="select-image-list">
        <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
        <div class="layui-col-sm3 layui-col-md3 layui-col-lg2">
            <div class="layui-card">
                <img src="<?php echo htmlentities($vo['url']); ?>">
                <span><i class="layui-icon layui-icon-ok"></i></span>
            </div>
        </div>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        <div class="pages"><?php echo $list; ?></div>
    </div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    var num = <?php echo htmlentities($num); ?>;
    if( num == 1 ) {
        $('.layui-card').click(function () {
            $('#select-image-list .layui-card').removeClass('active');
            $(this).addClass('active');
        })
    } else {
        $('.layui-card').click(function () {
            var class_name = $(this).attr('class');
            if( class_name.indexOf('active') ===-1 ) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        })
    }

</script>

</body>
</html>
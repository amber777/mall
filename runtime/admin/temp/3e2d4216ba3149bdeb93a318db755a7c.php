<?php /*a:2:{s:54:"C:\wamp64\www\other\wn\app\admin\view\banner\form.html";i:1606901413;s:49:"C:\wamp64\www\other\wn\app\admin\view\iframe.html";i:1584595684;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
    <style>
        .h15{height: 15px;}
    </style>
</head>
<body>
<div class="h15"></div>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-form" lay-filter="layuiadmin-app-form-list" id="layuiadmin-app-form-list">
            <form action="<?php echo request()->url(); ?>" method="post" class="xn_ajax" data-type="open">

                <div class="layui-form-item">
                    <label class="layui-form-label">图片标题</label>
                    <div class="layui-input-inline">
                        <input type="text" name="tittle" placeholder="图片标题" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['tittle']); ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片描述</label>
                    <div class="layui-input-inline">
                        <input type="text" name="desc" placeholder="图片描述,选填" autocomplete="off" class="layui-input" value="<?php echo htmlentities($data['desc']); ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片类型</label>
                    <div class="layui-input-inline">
                        <select name="type">
                            <?php foreach($banner_type as $key=>$vo): ?>
                                <option value="<?php echo htmlentities($key); ?>" <?php if($key == $data['type']): ?>selected="selected"<?php endif; ?>><?php echo htmlentities($vo); ?></option>
                            <?php endforeach; ?>
                        </select>

                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">图片上传</label>
                    <?php if($data['url'] != ''): ?>
                    <div class="layui-input-block ">
                        <input id="input_if_has_img" type="hidden" name="url" class="layui-input xn-images" value="<?php echo htmlentities($data['url']); ?>">
                        <img  id="if_has_img" src="<?php echo htmlentities($data['url']); ?>" width="150px" >
                        <span id="close_if_has_img" class="layui-icon layui-icon-close"></span>
                    </div>
                    <div class="layui-input-block"><span></span></div>
                    <?php else: ?>

                    <?php endif; ?>
                    <div class="layui-input-block">
                        <?php echo pic_choice_one($data['url'],'url'); ?>
                    </div>
                </div>


                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button type="submit" class="layui-btn">确定</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    //前端处理图片删除
    $(document).ready(function(){
        var close_btn = $("#close_if_has_img");
        close_btn.mouseover(function(){
            close_btn.css("cursor","pointer");
        });
        close_btn.on("click",function () {
            //删除input值
            $("#input_if_has_img").val("");
            //删除节点
            $("#if_has_img").remove();
            //删除本身
            this.remove();
            this.parent().remove();

        })
    });
</script>

</body>
</html>
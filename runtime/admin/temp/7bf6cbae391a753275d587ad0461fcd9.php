<?php /*a:3:{s:61:"C:\wamp64\www\other\wn\app\admin\view\upload_files\index.html";i:1606715393;s:47:"C:\wamp64\www\other\wn\app\admin\view\main.html";i:1606706270;s:53:"C:\wamp64\www\other\wn\app\admin\view\breadcrumb.html";i:1583051252;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="images/x-icon" href="<?php echo xn_cfg('base.logo'); ?>">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<?php if(!empty($breadcrumb)): ?>
<div class="layui-card layadmin-header">
    <div class="layui-breadcrumb" lay-filter="breadcrumb" style="visibility: visible;">
        <a href="<?php echo url('admin/index/home'); ?>">主页</a>
        <?php if(is_array($breadcrumb) || $breadcrumb instanceof \think\Collection || $breadcrumb instanceof \think\Paginator): if( count($breadcrumb)==0 ) : echo "" ;else: foreach($breadcrumb as $key=>$vo): ?>
        <a><cite><?php echo htmlentities($vo['title']); ?></cite></a>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>
<?php endif; ?>
<div class="layui-fluid">
    <div class="layui-card">
        <div class=" layui-card-header layuiadmin-card-header-auto">
            <form class="layui-form" method="get">



                <input type="hidden" name="bcid" value="<?php echo input('bcid'); ?>"><!--保留当前位置的bcid参数-->
                <div class="layui-form-item">


                    <div class="layui-inline">
                        <a href="<?php echo url('add'); ?>"  data-width="800px" data-height="650px" class="layui-btn   xn_open" title="添加图片">
                        <i class="layui-icon layui-icon-addition layuiadmin-button-btn"></i>
                        </a>
                    </div>

                    <div class="layui-inline">
                        <label class="layui-form-label">存储位置</label>
                        <div class="layui-input-inline">
                            <select name="storage">
                                <option value="">不限</option>
                                <option value="0" <?php if(input('storage')!='' and (input('storage') == 0)): ?> selected="selected" <?php endif; ?>>本地</option>
                                <option value="1" <?php if(input('storage')!='' and (input('storage') == 1)): ?> selected="selected" <?php endif; ?>>OSS</option>
                                <option value="2" <?php if(input('storage')!='' and (input('storage') == 2)): ?> selected="selected" <?php endif; ?>>七牛</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">开始日期</label>
                        <div class="layui-input-inline">
                            <input type="text" name="start_date" id="start_date" value="<?php echo input('start_date'); ?>" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input" >
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">截止日期</label>
                        <div class="layui-input-inline">
                            <input type="text" name="end_date" id="end_date" value="<?php echo input('end_date'); ?>" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <button class="layui-btn">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="layui-card-body">
            <table class="layui-table">
                <tr>
                    <th><b>图片</b></th>
                    <th><b>存储位置</b></th>
                    <th width="15%"><b>图片路径</b></th>
                    <th><b>应用</b></th>
                    <th><b>大小</b></th>
                    <th><b>创建时间</b></th>
                    <th><b>操作</b></th>
                </tr>

                <tbody>
                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><a href="javascript:;" onClick="showImg(this)" data-href="<?php echo htmlentities($vo['url']); ?>" ><img src="<?php echo htmlentities($vo['url']); ?>" width="80"></a></td>
                    <td><?php echo htmlentities($vo['storage']); ?></td>
                    <td><?php echo htmlentities($vo['url']); ?></td>
                    <td><?php echo htmlentities($vo['app']); ?></td>
                    <td><?php echo htmlentities($vo['file_size']); ?></td>
                    <td><?php echo htmlentities($vo['create_time']); ?></td>
                    <td>
                        <a href="<?php echo Url('delete',array('id'=>$vo['id'])); ?>" title="删除记录的同时将删除图片文件，确定要删除吗？"
                           class="layui-btn layui-btn-danger layui-btn-xs xn_delete">
                            <i class="layui-icon layui-icon-delete"></i>删除
                        </a>
                    </td>
                </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="pages"><?php echo $list; ?></div>
</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    layui.use('laydate', function(){
        var laydate = layui.laydate;

        laydate.render({
            elem: '#start_date'
        });
        laydate.render({
            elem: '#end_date'
        });
    });

    function showImg(obj){
        var url = $(obj).data('href');
        var thisimg = $(obj).siblings().find('img')
        var img = "<img src='" + url + "'  />";
        var setting = {
            type: 1,
            title: false,
            closeBtn: 0,
            skin: 'layui-layer-nobg', //没有背景色
            shadeClose: true,
            shade: 0.6, //遮罩透明度
            content: img
        }

        var windowH = $(window).height();
        var windowW = $(window).width();

        getImageWidth(url,function(w,h){
            //console.log("win:"+windowH+","+windowW);
            //console.log("img:"+h+","+w);
            // 调整图片大小
            if(w>windowW || h>windowH){
                if(w>windowW && h>windowH){
                    w = thisimg .width()*3;
                    h = thisimg .height()*3;
                    setting.area = [w+"px",h+"px"];
                }else if(w>windowW){
                    setting.area = [windowW+"px",windowW*0.5/w*h+"px"];
                }else{
                    setting.area = [w+"px",windowH+"px"];
                }
            }else{
                setting.area = [w+"px",h+"px"];
            }
            // 设置layer
            layer.open(setting);
        });
    }

    function getImageWidth(url,callback){
        var img = new Image();
        img.src = url;

        // 如果图片被缓存，则直接返回缓存数据
        if(img.complete){
            callback(img.width, img.height);
        }else{
            // 完全加载完毕的事件
            img.onload = function(){
                callback(img.width, img.height);
            }
        }


    }

</script>

</body>
</html>
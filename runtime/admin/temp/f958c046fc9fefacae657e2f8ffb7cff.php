<?php /*a:2:{s:71:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\index\index.html";i:1606122328;s:64:"C:\wamp64\www\other\xiaoniu_v1.20200319\app\admin\view\main.html";i:1606118849;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo xn_cfg('base.sys_name'); ?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script>
        //全局上传文件端口
        var UPLOAD_FILE_URL = "<?php echo url('upload_files/upload'); ?>";
        //全局选择文件端口
        var SELECT_FILE_URL = "<?php echo url('upload_files/select'); ?>";
    </script>
    <script src="/static/admin/js/jquery-2.0.0.min.js"></script>
    <script src="/static/admin/js/common.js"></script>
    <script src="/static/admin/js/upload.js"></script>
    <script src="/static/admin/js/webuploader.min.js"></script>
    <link rel="stylesheet" href="/static/admin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/layuiAdmin.css" media="all">
    <link rel="stylesheet" href="/static/admin/style/base.css" media="all">
    
</head>
<body>

<div id="LAY_app" class="layadmin-tabspage-none">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect="">
                    <a href="javascript:;" class="even_flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <!--<li class="layui-nav-item layui-this layui-hide-xs layui-hide-sm layui-show-md-inline-block">
                  <a lay-href="" title="">
                    控制台
                  </a>
                </li>-->
                <li class="layui-nav-item layui-hide-xs" lay-unselect="">
                    <a href="/" target="_blank" title="前台">
                        <i class="layui-icon layui-icon-website"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect="">
                    <a href="javascript:document.getElementById('content-iframe').contentWindow.location.reload();"
                       layadmin-event="refresh" title="刷新">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>

                <span class="layui-nav-bar" style="left: 198px; top: 48px; width: 0px; opacity: 0;"></span></ul>
                <ul class="layui-nav layui-layout-right">
                    <li class="layui-nav-item" >
                        <a href="javascript:;"  ><?php echo htmlentities($admin_data['username']); ?> </a>
                        <dl class="layui-nav-child">
                            <dd><a href="<?php echo url('admin/info',['id'=>$admin_data['id']]); ?>" target="right_content">修改资料</a></dd>
                            <dd><a href="<?php echo url('login/logout',['id'=>$admin_data['id']]); ?>">退出登录</a></dd>
                        </dl>

                    </li>

                    <li class="layui-nav-item"><a href="javascript:;"> </a></li>
                    <!-- 唤起侧边栏 解释说明
                    <li class="layui-nav-item" >
                        <a href="javascript:;" class="right-bar">
                            <i class="layui-icon layui-icon-more-vertical"></i>
                        </a>
                    </li>-->


                </ul>
        </div>

        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu">
            <div class="layui-side-scroll">
                <div class="layui-logo">
                    <span><?php echo xn_cfg('base.sys_name'); ?></span>
                </div>

                <ul class="layui-nav layui-nav-tree" lay-filter="test">
                    <!-- 侧边导航: <ul class="layui-nav layui-nav-tree layui-nav-side"> -->
                    <?php if(is_array($menu) || $menu instanceof \think\Collection || $menu instanceof \think\Paginator): if( count($menu)==0 ) : echo "" ;else: foreach($menu as $key=>$vo): if(!empty($vo['_data'])): ?>
                            <li data-name="" data-jump="" class="layui-nav-item">
                                <a href="javascript:;" lay-tips="<?php echo htmlentities($vo['title']); ?>">
                                    <i class="layui-icon <?php echo htmlentities($vo['icon']); ?>"></i> <cite><?php echo htmlentities($vo['title']); ?></cite>
                                    <span class="layui-nav-more"></span>
                                </a>

                                <dl class="layui-nav-child">
                                    <?php if(is_array($vo['_data']) || $vo['_data'] instanceof \think\Collection || $vo['_data'] instanceof \think\Paginator): if( count($vo['_data'])==0 ) : echo "" ;else: foreach($vo['_data'] as $key=>$vv): ?>
                                    <dd>
                                        <?php if(!empty($vv['_data'])): ?>
                                            <a href="javascript:;" target="right_content"><?php echo htmlentities($vv['title']); ?></a>
                                            <dl class="layui-nav-child">
                                            <?php if(is_array($vv['_data']) || $vv['_data'] instanceof \think\Collection || $vv['_data'] instanceof \think\Paginator): if( count($vv['_data'])==0 ) : echo "" ;else: foreach($vv['_data'] as $key=>$v): ?>
                                                <dd>
                                                     <a href="<?php echo url($v['name']); ?>?bcid=<?php echo htmlentities($vo['id']); ?>_<?php echo htmlentities($vv['id']); ?>_<?php echo htmlentities($v['id']); ?>" target="right_content"><?php echo htmlentities($v['title']); ?></a>
                                                </dd>
                                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                            </dl>
                                        <?php else: ?>
                                            <a href="<?php echo url($vv['name']); ?>?bcid=<?php echo htmlentities($vo['id']); ?>_<?php echo htmlentities($vv['id']); ?>" target="right_content"><?php echo htmlentities($vv['title']); ?></a>
                                        <?php endif; ?>
                                    </dd>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </dl>
                            </li>
                       <?php else: ?>
                            <li class="layui-nav-item">
                                <a href="<?php echo url($vo['name']); ?>?bcid=<?php echo htmlentities($vo['id']); ?>" target="right_content" lay-tips="<?php echo htmlentities($vo['title']); ?>">
                                    <i class="layui-icon <?php echo htmlentities($vo['icon']); ?>"></i>
                                    <cite><?php echo htmlentities($vo['title']); ?></cite>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>

        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body" style="overflow-y: hidden">
            <iframe id="content-iframe" src="<?php echo url('home'); ?>" frameborder="0" width="100%" height="100%" name="right_content" scrolling="auto" frameborder="0" scrolling="no"></iframe>
        </div>

        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>

    </div>

</div>


<script src="/static/admin/layui/layui.all.js"></script>
<script src="/static/admin/js/admin.js"></script>

<script>
    $('.even_flexible').click(function () {
        var i_obj = $(this).find('i');
        if( i_obj.attr('class').indexOf('layui-icon-shrink-right')==-1 ) {
            $('#LAY_app').removeClass('layadmin-side-shrink');
            i_obj.removeClass('layui-icon-spread-left').addClass('layui-icon-shrink-right')
        } else {
            $('#LAY_app').addClass('layadmin-side-shrink');
            i_obj.removeClass('layui-icon-shrink-right').addClass('layui-icon-spread-left')
        }
    });
    
    $('.layui-side-menu').click(function () {
        if( $('#LAY_app').attr('class').indexOf('layadmin-side-shrink')!=-1 ) {
            $('.even_flexible').trigger('click');
        }
    });
</script>

</body>
</html>

<!--    基础main导入 -->
<?php /*a:3:{s:52:"C:\wamp64\www\other\wn\app\index\view\news\info.html";i:1606961129;s:47:"C:\wamp64\www\other\wn\app\index\view\head.html";i:1606875676;s:47:"C:\wamp64\www\other\wn\app\index\view\foot.html";i:1606886285;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>吉程科技</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" type="images/x-icon" href="/uploads/jc_just_logo.png">
    <link rel="stylesheet" href="/static/index/css/bootstrap.css">
    <link rel="stylesheet" href="/static/index/css/base.css">
    <link rel="stylesheet" href="/static/index/css/index.css">

    <link rel="stylesheet" href="/static/index/layui/css/layui.css" media="all">


    <script type="text/javascript" src="/static/index/layui/layui.all.js"></script>
    <script type="text/javascript" src="/static/index/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/index/js/jqmain.js" charset="utf-8"></script>
<script >
    $(document).ready(function(){
        //加载指定栏目下标
        //$("#top_slide li:eq(0)").addClass('layui-this');
    })
</script>

</head>

<body>
<div id="_top"> </div>
<!--公用头部-->

        <ul class="layui-nav layui-bg-cyan" id="top_slide"    >
            <a href="/"><img style="cursor: pointer" src="/uploads/jc_logo_white.png" width="10%" id="top_logo" class="top_logo" style="display: inline"></a>
            <!--            顶级分类循环-->
            <?php if(is_array($column_list) || $column_list instanceof \think\Collection || $column_list instanceof \think\Paginator): $i = 0; $__LIST__ = $column_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <li class="layui-nav-item" style="font-size: 16px">

                    <a  href="<?php echo htmlentities($vo['url']); ?>" style="font-size: 16px"><?php echo htmlentities($vo['name']); if((isset($vo['child']) )): ?>
                            <dl class="layui-nav-child" style="font-size: 16px">
                                <?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$co): $mod = ($i % 2 );++$i;?>
                                    <a href="<?php echo htmlentities($co['url']); ?>" style="font-size: 16px"><?php echo htmlentities($co['name']); ?></a>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </dl>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>

<!--
                <li class="layui-nav-item"><a href="http://wn.com/">首页导航</a></li>
                <li class="layui-nav-item"><a href="" target="_blank" >解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="">移动模块</a></dd>
                        <dd><a href="">后台模版</a></dd>
                        <dd><a href="">电商平台</a></dd>
                    </dl>
                </li>
-->
        </ul>

<!--右下角工具栏-->
        <div id="top" class="hidden-xs   d2d2d2">
            <a class="layui-icon layui-icon-login-qq" href="tencent://message/?uin=&amp;在线咨询&amp;Menu=yes" target="_blank" title="QQ客服" id="qq"></a>
        <!--<a class="layui-icon layui-icon-chat" href="/join.html?#1" target="_blank" title="联系我们" id="contact-us"></a>-->
            <a class="layui-icon layui-icon-service" href="tel:15866666666" target="_blank" title="联系我们" id="wechat"></a>
            <a class="layui-icon layui-icon-up"   href="#_top" title="返回顶部" id="totop"  ></a>
        <!--<div id="code_img" ><img src=" /Uploads/qrcode.png" alt="公众号" class="img-responsive center-block"></div>-->
        </div>

        <div id="wn_main">




<!--文章详情news-->
<link rel="stylesheet" href="/static/index/css/news_content.css">
<div class="jumbotron jumbotron-news">
    <img src="<?php echo htmlentities($data['img_url']); ?>" width="100%" style="max-height: 500px">
</div>


<div id="content">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="nav">
                    <ol class="col-md-12 breadcrumb">
                        <li><a href="/index">首页</a></li>
                        <li><a href="/news">新闻中心</a></li>
                        <li class="active">文章详情</li>
                    </ol>
                </div>
                <div class="container-fluid content-container">
                    <div class="row info-content">
                        <h3 class="c_titile"><?php echo htmlentities($info['tittle']); ?></h3>
                        <p class="box_c">
                            <span></span>
                            <span class="d_time"><i class="fa fa-clock-o"></i> <?php echo htmlentities($info['create_time']); ?></span>
                            <span>浏览数:<?php echo htmlentities($info['view_num']+1); ?></span>
                        </p>
                        <article style="padding: 25px">
                            <?php echo $info['content']; ?>
                        </article>

                        <div class="line">站位</div>

                        <div class="ad"></div>
                        <div class="nextinfo">


                            <p>上一篇：
                                <a href="/13065.html">公司召开第四届四次董事会和2020年第二次股东会</a></p>
                            <p>下一篇：
                                没有更多了
                            </p>
                        </div>
                        <div class="otherlink">
                            <h4>相关文章</h4>
                            <ul>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="/12603.html" title="公司新一届党总支委员会成立">公司新一届党总支委员会成立</a>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <a href="/12604.html" title="三届一次董事会、监事会顺利召开">三届一次董事会、监事会顺利召开</a>
                                </li>

                            </ul>
                        </div>

                        <div class="end">吉程科技有限公司</div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="margin-bottom"></div>
<div class="clearfix"></div>
<div class="margin-bottom"></div>
<div class="clearfix"></div>




</div>

<footer id="footer" class="footer-container">
    <div class="container">
        <div class="row footer-margin">
            <div class="col-md-12 footer-top">
                <div class="thought">
                    <h3>现代化生活服务企业 客户专心工作的系统助理</h3>
                    <p>Modern life service enterprises Customer care system assistant</p>
                </div>
                <ul>
                    <li><a href="/joinUs" target="_blank">联系我们</a><span>|</span></li>
                    <li><a href="/friendLink">友情链接</a></li>
                    <!--
                    <li><a href="#">网站地图</a><span>|</span></li>
                    <li><a href="#">隐私安全</a></li>
                    -->
                </ul>
            </div>
            <div class="footer-line">站位</div>
            <div class="col-md-6 col-sm-12 qrcode">
                <img src="/Uploads/qrcode.png" width="120px" alt="公众号" class="img-responsive center-block">
            </div>
            <div class="col-md-6 col-sm-12 footer-bottom-right">
                <p class="footer-box">
                    <span><i class="fa fa-phone"></i> 电话：&nbsp;<a href="tel:028-87399999">028-87399999</a></span>　
                    <span><i class="fa fa-envelope-o"></i> 邮箱：</span>
                </p>
                <p class="footer-box">  版权所有：&nbsp;©北京吉程科技金融有限公司  </p>
                <p class="footer-box">
                    地址：&nbsp;四川省成都市浣花北路1号e座2楼                </p>
                <p class="footer-box">
                    邮编：&nbsp;610072　备案信息：蜀ICP备15011477号                </p>
            </div>

        </div>
    </div>
</footer>
<script type="text/javascript" src="/static/index/js/index.js" charset="utf-8"></script>
<script type="text/javascript" src="/static/index/js/bootstrap.js"></script>

<script type="text/javascript">

    $(function(){
        $('#top_slide li  ').click(function(){

            $('#top_slide li').removeClass('layui-this');
            $(this).addClass('layui-this');


        });

    });



</script>


<script>
    layui.use('element', function () {
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

        //监听导航点击
        element.on('nav(demo)', function (elem) {
            //console.log(elem)
            layer.msg(elem.text());
        });
    });


    layui.use(['carousel', 'form'], function () {
        var carousel = layui.carousel
            , form = layui.form;

        //轮播设定各种参数
        carousel.render({
            elem: '#banner'
            , width: '100%'
            , height: '680px'
            , interval: 3000
        });
    });

</script>

<script type="text/javascript">

    var xsBoxWidth = $('.xs-box').width();
    $('.xs-p-a').width(xsBoxWidth);


    //定位
    /*
    $('.navbar-nav li').click(function() {
        var index = $(this).index();
        //获取对应区域的高度
        var h = $('.projects').eq(index).offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });
    */

    /*
    $('.project_show_item').click(function() {
        //获取对应区域的高度
        var h = $('.project-display').offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });

    $('.service_item').click(function() {
        //获取对应区域的高度
        var h = $('.scope').offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });
    */

</script>
</body>
</html>
<?php /*a:3:{s:54:"C:\wamp64\www\other\wn\app\index\view\index\index.html";i:1606962716;s:47:"C:\wamp64\www\other\wn\app\index\view\head.html";i:1606875676;s:47:"C:\wamp64\www\other\wn\app\index\view\foot.html";i:1606886285;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>吉程科技</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" type="images/x-icon" href="/uploads/jc_just_logo.png">
    <link rel="stylesheet" href="/static/index/css/bootstrap.css">
    <link rel="stylesheet" href="/static/index/css/base.css">
    <link rel="stylesheet" href="/static/index/css/index.css">

    <link rel="stylesheet" href="/static/index/layui/css/layui.css" media="all">


    <script type="text/javascript" src="/static/index/layui/layui.all.js"></script>
    <script type="text/javascript" src="/static/index/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/index/js/jqmain.js" charset="utf-8"></script>
<script >
    $(document).ready(function(){
        //加载指定栏目下标
        //$("#top_slide li:eq(0)").addClass('layui-this');
    })
</script>

</head>

<body>
<div id="_top"> </div>
<!--公用头部-->

        <ul class="layui-nav layui-bg-cyan" id="top_slide"    >
            <a href="/"><img style="cursor: pointer" src="/uploads/jc_logo_white.png" width="10%" id="top_logo" class="top_logo" style="display: inline"></a>
            <!--            顶级分类循环-->
            <?php if(is_array($column_list) || $column_list instanceof \think\Collection || $column_list instanceof \think\Paginator): $i = 0; $__LIST__ = $column_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <li class="layui-nav-item" style="font-size: 16px">

                    <a  href="<?php echo htmlentities($vo['url']); ?>" style="font-size: 16px"><?php echo htmlentities($vo['name']); if((isset($vo['child']) )): ?>
                            <dl class="layui-nav-child" style="font-size: 16px">
                                <?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['child'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$co): $mod = ($i % 2 );++$i;?>
                                    <a href="<?php echo htmlentities($co['url']); ?>" style="font-size: 16px"><?php echo htmlentities($co['name']); ?></a>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </dl>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endforeach; endif; else: echo "" ;endif; ?>

<!--
                <li class="layui-nav-item"><a href="http://wn.com/">首页导航</a></li>
                <li class="layui-nav-item"><a href="" target="_blank" >解决方案</a>
                    <dl class="layui-nav-child">
                        <dd><a href="">移动模块</a></dd>
                        <dd><a href="">后台模版</a></dd>
                        <dd><a href="">电商平台</a></dd>
                    </dl>
                </li>
-->
        </ul>

<!--右下角工具栏-->
        <div id="top" class="hidden-xs   d2d2d2">
            <a class="layui-icon layui-icon-login-qq" href="tencent://message/?uin=&amp;在线咨询&amp;Menu=yes" target="_blank" title="QQ客服" id="qq"></a>
        <!--<a class="layui-icon layui-icon-chat" href="/join.html?#1" target="_blank" title="联系我们" id="contact-us"></a>-->
            <a class="layui-icon layui-icon-service" href="tel:15866666666" target="_blank" title="联系我们" id="wechat"></a>
            <a class="layui-icon layui-icon-up"   href="#_top" title="返回顶部" id="totop"  ></a>
        <!--<div id="code_img" ><img src=" /Uploads/qrcode.png" alt="公众号" class="img-responsive center-block"></div>-->
        </div>

        <div id="wn_main">







<div class="layui-carousel" id="banner" lay-filter="test4">
    <div carousel-item="">
        <?php if(is_array($banner_list) || $banner_list instanceof \think\Collection || $banner_list instanceof \think\Paginator): $i = 0; $__LIST__ = $banner_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
        <div><img src="<?php echo htmlentities($vo['url']); ?>"><?php echo htmlentities($vo['tittle']); ?></div>
        <?php endforeach; endif; else: echo "" ;endif; ?>
    </div>
</div>

<div id="container_box">

    <div class="projects hidden-xs hidden-sm"></div>
    <div class="news visible-lg visible-md">
        <div class="news-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 " style="padding:0;margin:0">
                        <div class="media media-title">
                           <div class="media-left">
                                <span>N</span>
                            </div>
                            <div class="media-body">
                                <p class="media-heading">EWS CENTER</p>
                                <p class="text-muted">新闻中心　>></p>
                            </div>
                       
                        </div>
                    </div>
                    <div class="col-md-12 news-type-name">
                        <span class="col-md-4">公司动态</span>
                        <span class="col-md-4">行业新闻</span>
                        <span class="col-md-4">通知公告</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="jumbotron news-body">

            <div class="container">
                <div class="col-md-4"><img src="http://www.cdhhsy.com.cn/Public/Home/images/pic7.jpg" alt="公司动态"></div>
                <div class="col-md-4"><img src="http://www.cdhhsy.com.cn/Public/Home/images/pic8.jpg" alt="行业新闻"></div>
                <div class="col-md-4"><img src="http://www.cdhhsy.com.cn/Public/Home/images/pic9.jpg" alt="通知公告"></div>
            </div>
        </div>
        <div class="col-md-12 news-footer">
            <div class="container">
                <div class="col-md-4">
                    <ul>
                        <li><em class="visible-lg">2020-10-30</em><a href="/13067.html" target="_blank">公司召开员工大会</a>
                        </li>
                        <li><em class="visible-lg">2020-10-09</em><a href="/13065.html" target="_blank">公司召开第四届四次董事会和2020年第二次股东会</a>
                        </li>
                        <li><em class="visible-lg">2020-09-18</em><a href="/13064.html" target="_blank">公司召开班子工作汇报会</a>
                        </li>
                        <li><em class="visible-lg">2020-08-28</em><a href="/13063.html"
                                                                     target="_blank">公司召开项目管理反思分析会</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <a class="news-btn" href="/c-company-news.html" target="_blank">更多 >></a>
                </div>
                <div class="col-md-4">
                    <ul>
                        <li><em class="visible-lg">2020-08-21</em><a href="/13057.html" target="_blank">六部门印发关于进一步优化企业开办服务的通知</a>
                        </li>
                        <li><em class="visible-lg">2020-05-29</em><a href="/13060.html" target="_blank">将物业服务企业纳入社区治理体系——央视新闻联播播出两会代表提案</a>
                        </li>
                        <li><em class="visible-lg">2020-04-22</em><a href="/13061.html" target="_blank">一大波很实用的物业服务权责划分常识问答</a>
                        </li>
                        <li><em class="visible-lg">2020-03-18</em><a href="/13046.html"
                                                                     target="_blank">人民日报：武汉雷神山医院物业团队</a></li>
                    </ul>
                    <a class="news-btn" href="/c-trade-news.html" target="_blank">更多 >></a>
                </div>
                <div class="col-md-4">
                    <ul>
                        <li><em class="visible-lg">2020-10-16</em><a href="/13066.html" target="_blank">【办公地点变更通告】成都金浣花实业有限公司办公地点变更通告</a>
                        </li>
                        <li><em class="visible-lg">2020-07-03</em><a href="/13052.html" target="_blank">关于水韵尚城住宅2020年物业费收取通知</a>
                        </li>
                        <li><em class="visible-lg">2020-06-12</em><a href="/13050.html"
                                                                     target="_blank">温江水韵尚城绿化养护项目招标公告</a></li>
                        <li><em class="visible-lg">2020-01-16</em><a href="/13039.html"
                                                                     target="_blank">招标公告-水韵尚城消防维保</a></li>
                    </ul>
                    <a class="news-btn" href="/c-announcement.html" target="_blank">更多 >></a>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <a name="1"></a>
    <div id="myCarousel" class="carousel slide projects projects-wrapper project-display">
        <div class="container container-none">
            <div class="media media-title">
                <div class="media-left">
                    <span>P</span>
                </div>
                <div class="media-body">
                    <p class="media-heading">ROJECT DISPLAY</p>
                    <p class="text-muted">项目展示　>></p>
                </div>
            </div>
        </div>
        <ol class="carousel-indicators">
            <?php if(is_array($project) || $project instanceof \think\Collection || $project instanceof \think\Paginator): $i = 0; $__LIST__ = $project;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li data-target="#myCarousel" data-slide-to="<?php echo htmlentities($key); ?>" class="<?php if($key == 0): ?>active<?php endif; ?>"></li>&nbsp;
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ol>

        <div class="container">

            <div class="carousel-inner">
                <?php if(is_array($project) || $project instanceof \think\Collection || $project instanceof \think\Paginator): $i = 0; $__LIST__ = $project;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                <div class="item <?php if($key == 0): ?>active<?php endif; ?>" style="background-color:#fff;min-height: 488px;">
                    <a href="<?php echo htmlentities($vo['id']); ?>.html" target="_blank">
                        <img src="<?php echo htmlentities($vo['img']); ?>"
                             alt="<?php echo htmlentities($vo['tittle']); ?>">
                    </a>
                    <div class="carousel-caption">
                        <div class="intro">
                            <h3><?php echo htmlentities($vo['tittle']); ?></h3>
                            <p><?php echo htmlentities($vo['desc']); ?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </div>

        <a href="#myCarousel" data-slide="prev" class="carousel-control left">
            <span class="layui-icon  layui-icon-left" id="index_middle_left" ></span>
        </a>
        <a href="#myCarousel" data-slide="next" class="carousel-control right">
            <span class="layui-icon  layui-icon-right" id="index_middle_right"></span>
        </a>
    </div>

    <a name="2"></a>
    <div id="information" class="scope">
        <div class="projects service-items">
            <div class="container container-none">
                <div class="row margin-right-15">
                    <div class="col-md-12">
                        <div class="media media-title">
                            <div class="media-left">
                                <span>S</span>
                            </div>
                            <div class="media-body">
                                <p class="media-heading">COPE OF BUSINESS</p>
                                <p class="text-muted">业务范围　>></p>
                            </div>
                        </div>
                    </div>


                    <?php if(is_array($business) || $business instanceof \think\Collection || $business instanceof \think\Paginator): $i = 0; $__LIST__ = $business;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <div class="col-sm-6 col-md-4 service-item">
                        <div class="thumbnail">
                            <img src="<?php echo htmlentities($vo['img']); ?>"
                                 alt="<?php echo htmlentities(mb_substr($vo['content'],0,50)); ?>">

                            <a href="/<?php echo htmlentities($vo['id']); ?>.html" target="_blank">

                                <div class="caption-wrapper">
                                    <div class="caption">
                                        <h3 class="title-hide">
                                            <span class="col-xs-12 chinese"><?php echo htmlentities($vo['tittle']); ?></span>
                                            <span class="col-xs-12 english"><?php echo htmlentities($vo['desc']); ?></span>
                                        </h3>
                                        <div class="service-content">
                                            <h3 class="title">
                                                <span class="col-xs-12 chinese"><?php echo htmlentities($vo['tittle']); ?></span>
                                                <span class="col-xs-12 english"><?php echo htmlentities($vo['desc']); ?><span
                                                        class="bottom-line"></span></span>
                                            </h3>
                                            <div class="clearfix"></div>
                                            <p class="text">
                                                <?php echo mb_substr($vo['content'],0,150); ?>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                    <!--
                    <div class="col-sm-6 col-md-4 service-item">
                        <div class="thumbnail">
                            <img src="http://www.cdhhsy.com.cn/Uploads/Service/2017-12-28/5a44a6c6cdd42.jpg" alt="公司全资子公司-中电建商旅旅行社成都有限公司致力于为客户提供优质、便捷、高效的商旅出行服务，目前已为包括电建集团、川投集团等数十万客户提供商旅出行服务。
    中电建商旅旅行社成都有限公司与各大航空公司开展友好合作。2019年先后荣获中国南方航空股份有限公司四川分公司优秀国内销售合作伙伴、中国国际...">

                            <a href="/page-1.html" target="_blank">

                                <div class="caption-wrapper">
                                    <div class="caption">
                                        <h3 class="title-hide">
                                            <span class="col-xs-12 chinese">商旅服务</span>
                                            <span class="col-xs-12 english">ELERONIC COMMERCE</span>
                                        </h3>
                                        <div class="service-content">
                                            <h3 class="title">
                                                <span class="col-xs-12 chinese">商旅服务</span>
                                                <span class="col-xs-12 english">ELERONIC COMMERCE<span
                                                        class="bottom-line"></span></span>
                                            </h3>
                                            <div class="clearfix"></div>
                                            <p class="text">
                                                公司全资子公司-中电建商旅旅行社成都有限公司致力于为客户提供优质、便捷、高效的商旅出行服务，目前已为包括电建集团、川投集团等数十万客户提供商旅出行服务。
                                                中电建商旅旅行社成都有限公司与各大航空公司开展友好合作。2019年先后荣获中国南方航空股份有限公司四川分公司优秀国内销售合作伙伴、中国国际...</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div> -->


                </div>
            </div>
        </div>
    </div>



    <a name="3"></a>
    <div class="jumbotron partner" style="background:url(/static/index/img/partner1.jpg);background-size: cover;background-position:0rem -20rem;">
        <div class="container container-none">
            <div class="col-sm-12 col-md-5 container-none">
                <div class="media media-title">
                    <div class="media-left">
                        <span>C</span>
                    </div>
                    <div class="media-body">
                        <p class="media-heading">OOPREATIVE PARTNER</p>
                        <p class="text-muted">合作伙伴　>></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12  col-xs-12">

                <!--
                    中国银联
                    中国联通
                    百度
                    雍和宫
                    国家电网
                    北京市电信工程局
                    360
                -->
                <div id="logo-box" class="hidden-xs">

                    <div class='list'>
                        <a target="_blank" title="中国银联" href="https://cn.unionpay.com/"><img  src='/static/index/img/yinlian.png'/></a>                            </div><div class='list'>
                        <a target="_blank" title="中国联通" href="http://www.chinaunicom.com.cn/"><img src='/static/index/img/liantong.jpg'/></a>                            </div><div class='list'>
                        <a target="_blank" title="百度" href="https://www.baidu.com/"><img style="margin-top: -30px;" src='/static/index/img/baidu.png'/></a>                            </div><div class='list'>
                        <a target="_blank" title="雍和宫" href="http://www.yonghegong.cn/"><img style="width: 52%;" src='/static/index/img/yonghegong.png'/></a>                            </div><div class='list'>
                        <a target="_blank" title="国家电网" href="http://www.sgcc.com.cn/"><img style="width: 82%;margin-top: -14px;" src='/static/index/img/guojiadianwang.jpg'/></a>                            </div><div class='list'>
                        <a target="_blank" title="北京市电信工程局" href="http://www.bteb.cn/"><img src='/static/index/img/bteb.jpg'/></a>                            </div><div class='list'>
                        <a target="_blank" title="360" href="http://www.360.cn/"><img src='/static/index/img/360.png'/></a>
                    </div>
                    <div class='list dis-none'  > <a target="_blank" title="qita" href="http://www.qq.cn/"><img src='/static/index/img/yinlian.png'/></a> </div>
                </div>
                    <div class="clearfix" style="margin-bottom:15px;"></div>


                <div class="visible-xs">

                    <!--<div class='xs-box col-xs-4' style="margin: 0 0 10px 0;padding:0 5px;">
                    <a href="http://www.xcc.sc.cn/"><img style="height: 80px" src='http://www.cdhhsy.com.cn/Uploads/Links/2019-03-19/5c909031d2185.jpg'/></a>
                    </div>
                    <div class='xs-box col-xs-4' style="margin: 0 0 10px 0;padding:0 5px;">
                        <a href="http://www.xcc.sc.cn/"><img style="height: 80px" src='http://www.cdhhsy.com.cn/Uploads/Links/2019-03-19/5c909031d2185.jpg'/></a>
                    </div>-->
                </div>
                </div>
            </div>
        </div>
    </div>



<div class="margin-bottom"></div>
<div class="clearfix"></div>
</div>

</div>





</div>

<footer id="footer" class="footer-container">
    <div class="container">
        <div class="row footer-margin">
            <div class="col-md-12 footer-top">
                <div class="thought">
                    <h3>现代化生活服务企业 客户专心工作的系统助理</h3>
                    <p>Modern life service enterprises Customer care system assistant</p>
                </div>
                <ul>
                    <li><a href="/joinUs" target="_blank">联系我们</a><span>|</span></li>
                    <li><a href="/friendLink">友情链接</a></li>
                    <!--
                    <li><a href="#">网站地图</a><span>|</span></li>
                    <li><a href="#">隐私安全</a></li>
                    -->
                </ul>
            </div>
            <div class="footer-line">站位</div>
            <div class="col-md-6 col-sm-12 qrcode">
                <img src="/Uploads/qrcode.png" width="120px" alt="公众号" class="img-responsive center-block">
            </div>
            <div class="col-md-6 col-sm-12 footer-bottom-right">
                <p class="footer-box">
                    <span><i class="fa fa-phone"></i> 电话：&nbsp;<a href="tel:028-87399999">028-87399999</a></span>　
                    <span><i class="fa fa-envelope-o"></i> 邮箱：</span>
                </p>
                <p class="footer-box">  版权所有：&nbsp;©北京吉程科技金融有限公司  </p>
                <p class="footer-box">
                    地址：&nbsp;四川省成都市浣花北路1号e座2楼                </p>
                <p class="footer-box">
                    邮编：&nbsp;610072　备案信息：蜀ICP备15011477号                </p>
            </div>

        </div>
    </div>
</footer>
<script type="text/javascript" src="/static/index/js/index.js" charset="utf-8"></script>
<script type="text/javascript" src="/static/index/js/bootstrap.js"></script>

<script type="text/javascript">

    $(function(){
        $('#top_slide li  ').click(function(){

            $('#top_slide li').removeClass('layui-this');
            $(this).addClass('layui-this');


        });

    });



</script>


<script>
    layui.use('element', function () {
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块

        //监听导航点击
        element.on('nav(demo)', function (elem) {
            //console.log(elem)
            layer.msg(elem.text());
        });
    });


    layui.use(['carousel', 'form'], function () {
        var carousel = layui.carousel
            , form = layui.form;

        //轮播设定各种参数
        carousel.render({
            elem: '#banner'
            , width: '100%'
            , height: '680px'
            , interval: 3000
        });
    });

</script>

<script type="text/javascript">

    var xsBoxWidth = $('.xs-box').width();
    $('.xs-p-a').width(xsBoxWidth);


    //定位
    /*
    $('.navbar-nav li').click(function() {
        var index = $(this).index();
        //获取对应区域的高度
        var h = $('.projects').eq(index).offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });
    */

    /*
    $('.project_show_item').click(function() {
        //获取对应区域的高度
        var h = $('.project-display').offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });

    $('.service_item').click(function() {
        //获取对应区域的高度
        var h = $('.scope').offset().top;
        $('html,body').animate({'scrollTop': h-50}, 500);
    });
    */

</script>
</body>
</html>
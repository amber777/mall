<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 是否启用事件
    'with_event'       => true,
    // 默认应用
    'default_app'      => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [
        //'*'     =>'index',
        //'admin' =>'admin',
    ],
    //自动加载多应用
    'auto_multi_app'=>true,
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    'dispatch_success_tmpl' => app()->getRootPath() . 'view/tpl/dispatch_jump.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => false,

    //轮播图类型
    'BANNER_TYPE'=>[
        '1'=>'首页大图',
        '2'=>'项目展示',
        '3'=>'新闻背景',

    ],

    //文章类型
    'NEWS_TYPE'=>[
        '1'=>'新闻中心',
        '2'=>'公司动态',
        '3'=>'行业新闻',
        '4'=>'通知公告',
        '5'=>'招聘信息',
        '6'=>'项目展示',
        '7'=>'业务范围',

    ],

];

<?php
return [
	'storage' => '0',
	'oss_ak' => '',
	'oss_sk' => '',
	'oss_endpoint' => '',
	'oss_bucket' => '',
	'qiniu_ak' => '',
	'qiniu_sk' => '',
	'qiniu_domain' => '',
	'qiniu_bucket' => '',
	'water_img' => '/uploads/jc_logo.png',
	'water_locate' => '9',
	'water_alpha' => '40',
];
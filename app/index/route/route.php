<?php
use think\facade\Route;



//前端页面路由绑定
//文章主页

Route::rule('news', 'news/index');

//文章详情
Route::get('news/:id', 'news/info')->pattern(['id' => '\d+']);
Route::get('/:id', 'news/info')->pattern(['id' => '\d+']);

//其他文章
Route::get('/id','news/info');
<?php
namespace app\index\controller;


use app\index\controller\Base;
use app\common\model\Column;
use think\App;

class Index extends Base
{


    //网站首页
    public function index()
    {
        //return $this->role();
        //栏目数据
        $top_list=$this->column_list;
        //轮播图   1首页大图 2项目展示 3新闻插图
        $banner_list=$this->getBanner(1);
        //项目展示
        $project_list = News::get_news_list(6);
        //业务范围
        $business = News::get_news_list(7,['id','tittle','img','desc','content'],['id'=>'asc']);

        //首页数据
        $data = [
            'column_list'=>$top_list,
            'banner_list'=>$banner_list,
            'project'=>$project_list,
            'business'=>$business,
        ];

        return view('',$data);

    }




    public function role(){
        //角色表
        $role=[
            ['role_id'=>1,'openid'=>'admin','role_name'=>'admin','role_level'=>1 ],
            ['role_id'=>2,'openid'=>'user','role_name'=>'user','role_level'=>2 ],
            ['role_id'=>3,'openid'=>'member','role_name'=>'member','role_level'=>3 ],
        ];
        //菜单
        $menu=[
            ['menu_id'=>1,'','menu_name'=>'余额充值','menu_level'=>1,'url'=>''],
            ['menu_id'=>2,'','menu_name'=>'充值记录','menu_level'=>1,'url'=>''],
            ['menu_id'=>3,'','menu_name'=>'消费记录','menu_level'=>1,'url'=>''],
            ['menu_id'=>3,'','menu_name'=>'工作审核','menu_level'=>2,'url'=>''],
            ['menu_id'=>3,'','menu_name'=>'我是员工','menu_level'=>2,'url'=>''],
        ];

        $openid =  $this->request->get('openid');
        //dd($this->request->get());
        //判断登入者身份
        $user = [];
        foreach ($role as $v){
            if(in_array($openid,$v)){
                $user=$v;
            }
        }
        //组装权限
        $has_menu = [];
        foreach ($menu as $mv){
            if($user['role_level']==$mv['menu_level']){
                $has_menu[] = $mv;
            }
        }
        return json($has_menu);
    }

    //吉程科技
    public function jk(){
        //获取参数
        $id= $this->request->get();
        //参数过滤
        $ids = version();


    }








}
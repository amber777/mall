<?php
namespace app\index\controller;

use app\index\controller\Base;
use app\common\model\News as NewsModel;
use think\Config;
use think\facade\Db;

class News extends Base {



    private static $table_name = 'news';
    private static $pk = 'id';

    public function index(){

        //dd($this->get_info(1));

        return view('',['column_list'=>$this->column_list]);
    }




    /**   根据类型获取文章列表 默认类型为新闻
     * @param int $type
     * @param array $field
     * @param array $order
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
             //文章类型
            'NEWS_TYPE'=>[
            '1'=>'新闻中心',
            '2'=>'公司动态',
            '3'=>'行业新闻',
            '4'=>'通知公告',
            '5'=>'招聘信息',
            '6'=>'项目展示',
            '7'=>'业务范围',
            ],
     */
    public static function get_news_list($type=1,$field=[],$order=[]){
        $model = new NewsModel();
        //默认条件 状态正常 新闻类型
        $where = ['status'=>1,'type'=>$type];
        //默认字段
        $field = empty($field)?['id','tittle','img','type','desc','from','view_num','create_time']:$field;
        //默认sort排序
        $order = empty($order)?['sort'=>'desc','id'=>'desc']:$order;
        //分页
        $page  = [];
        //返回数据
        $list  = $model->field($field)->where($where)->order($order)->select()->toArray();
        return $list;

    }


    //展示文章到详情页
    public function info($id){
        //dd($id);
        $info = $this->get_info($id);
        if($info==false){
            return view('error/404');
        }
        //dd($info);

        $img_url=Db::name('Banner')->field('url')->where(['type'=>'3'])->find();

        $data = ['img_url'=>$img_url['url']];

        return view('info',['info'=>$info,'column_list'=>$this->column_list,'data'=>$data]);



    }

    //获取一条数据详情
    public function get_info($id){
        if(isset($id) && !empty($id) ){
            $id=intval($id);
            if( ! is_int($id)){
                return false;
            }
            $info = Db::name(self::$table_name)->where(['status'=>1])->find( $id);
            if($info){
                //浏览+1
                Db::name(self::$table_name)->where(['id'=>$id])->inc( 'view_num','1')->update();
                return $info;
            }
            return false;
        }
        return false;
    }
}

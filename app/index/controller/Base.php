<?php


namespace app\index\controller;

use app\common\model\Column ;
use think\App;
use think\facade\Db;

class Base extends \app\common\controller\Base
{

    //前端自定义基础控制器
    protected $column_list;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->column_list=$this->getTopList();
    }



    //栏目数据
    protected function getTopList(){

        //考虑写进缓存 后台操作时修改缓存即可
       /*
            $redis = new Redis;
           if($redis->get('TopList')){
                return ;
            }else{
                //查询
            }
       */
        //栏目数据
        $columnModel = new Column();
        $field=['id','name','type','url','pid','level'];
        $column_list = $columnModel->field($field)->where(['status'=>1])->order('sort','desc') ->select()->toArray();

        //dd($column_list);
        //分开顶级分类和二级分类
        $top_list=[];
        $child_list=[];
        foreach ( $column_list as $k=>$v){
            //次级分类
            if($v['pid']!==0){
                //dd($v);
                $child_list[] = $v;
            }
            //顶级分类
            else{
                $top_list[]=$v;
            }
        }

        //次级合并到顶级
        foreach ($child_list as $k=>$v){
            foreach ($top_list as $j=>$k){
                if($v['pid']==$k['id']){
                    $top_list[$j]['child'][]=$v;
                }
            }
        }
        return $top_list;
    }


    //根据图片类型返回图片
    protected function getBanner($type=1){
        $field = ['id','url','tittle','type','desc','sort'];
        $where = ['status'=>1,'type'=>$type];

        $banner_list = Db::name('banner')->field($field)->where($where)->select()->toArray();
        return $banner_list;
    }



    public function __call($method, $args)
    {

        return  "<script>alert('请求不正确!'); window.history.go(-1)</script>";
    }
}
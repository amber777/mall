<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\facade\Db;
use think\facade\Config;
use app\common\model\Banner as BannerModel;
class Banner extends AdminBase
{
//todo banner 轮播图后台管理

    /*
      public function select()
    {
        $list = Db::name()
                    ->where('app',1)
                    ->where('user_id',$this->getAdminId())
                    ->order('id desc')
                    ->paginate(['query' => $this->request->param()]);
        $num = intval($this->request->get('num'));
        return view('',['list'=>$list,'num'=>$num]);
    }
      */
    //首页
    public function index()
    {


        //获取配置信息->轮播图类型
        $banner_type = Config::get('app.BANNER_TYPE');
        //dd($banner_type);
        $model = new BannerModel();
        //构造搜索条件
        $param = $this->request->param();
        if( $param['type']!='' ) {
            $model = $model->where('type',$param['type']);
        }

        $list = $model->order('id desc')->paginate(['query' => $param]);
        return view('',['list'=>$list,'banner_type'=>$banner_type]);
    }


    //添加
    public function add(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            if (  $param['tittle']==''){
                $this->error('图片标题不能为空');
            }
            $logo= isset($param['url'])?$param['url']:'';
            if(empty($logo)){
                $this->error('请选择图片');
            }
            $logo=explode(',',$logo);
            $url=[];
            foreach ($logo as $k=>$v){
                if( ! empty($v)){
                    $url[]=$v;
                }
            }

            if(count($url)!==1){
                $this->error('只能选择一张图片');
            }
            $param['url']=$url[0];
            $param['create_time']=date('Y-m-d H:i:s',time());
            //dd($param);

            $res = Db::name('banner')->insert($param);
            if($res){
                $this->success('添加成功!');
            }else{
                $this->error('添加失败 , 请稍后重试~!');
            }
        }

        //获取要修改的数据信息->find
        $data = [];
        if($this->request->get('id')){
            $data=Db::name('banner')->find($this->request->get('id'));
        }
        //获取图片类型
        $banner_type = Config::get('app.BANNER_TYPE');
        return view('form',['data'=>$data,'banner_type'=>$banner_type]);
    }



    //删除数据
    public function delete()
    {
        $id = intval($this->request->get('id'));

        !($id>0) && $this->error('参数错误');

        if(Db::name('banner')-> where(['id'=>$id])->delete()){
            $this->success('删除成功');
        }

    }



    // 启用|禁用数据
    public function set_status()
    {

        if( $this->request->isPost() ) {
            $param = $this->request->param();
            //dd($param);
            $res = Db::name('banner')->save($param);
            if($res){
                $this->success($param['status']==0?'已禁用':'已启用');
            }
        }

    }

    //修改
    public function edit(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();

            $img= isset($param['url'])?$param['url']:'';
            //文章可以不传封面图
            if( ! empty($img)){
                $img=explode(',',$img);
                $url=[];
                foreach ($img as $k=>$v){
                    if( ! empty($v)){
                        $url[]=$v;
                    }
                }

                if(count($url)>1){
                    $this->error('只能选择一张封面图片');
                }
                $param['url']=$url[0];
            }else{
                $this->error('未选择封面');
            }

            //dd($param);
            $res = Db::name('banner')->save($param);
            if($res){
                $this->success('修改成功');
            }
        }
        $data = [];
        $id = $this->request->get('id');
        if($id){
            $data=Db::name('banner')->find($id);
        }
        //图片类型
        $type_list = Config::get('app.BANNER_TYPE');
        return view('form',['data'=>$data,'banner_type'=>$type_list]);
    }

    //banner test
    public function test(){
        //扩展类

    }



}

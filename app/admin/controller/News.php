<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\facade\Db;
use think\facade\Config;
use think\facade\Session;
use app\common\model\News as NewsModel;
class News extends AdminBase
{

//TODO News 文章后台管理


    //表名
    private static $table_name='news';
    //主键
    private static $pk='id';

    //首页
    public function index()
    {
        //dd($_SERVER);

        //获取所有文章类型
        $news_type_list = Config::get('app.NEWS_TYPE');
        //dd(self::$table_name);
        $model = new NewsModel();
        //构造搜索条件
        $param = $this->request->param();
        if( $param['type']!='' ) {
            $model = $model->where('type',$param['type']);
        }
        if( $param['tittle']!='' ) {
            $model = $model->where('tittle','like','%'.$param['tittle'].'%');
        }

        //分页查询
        $list = $model->order(['sort'=>'desc','id'=>'desc'])->paginate(['query' => $param]);
        //$last_sql = $model->getLastSql();
        return view('',['list'=>$list,'news_type'=>$news_type_list ]);
    }


    //添加
    public function add(){
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            if (  $param['tittle']==''){
                $this->error('文章标题不能为空');
            }
            if ( ! $param['content'] ){
                $this->error('文章内容不能为空');
            }
            if (  $param['type']==''){
                $this->error('请选择文章类型');
            }
            if (  $param['from']==''){
                $this->error('若没有文章来源 , 请写原创');
            }
            if (  $param['author']==''){
                $this->error('请填写文章作者名字');
            }
            $img= isset($param['img'])?$param['img']:'';
            //文章可以不传封面图
            if( ! empty($img)){
                $img=explode(',',$img);
                $url=[];
                foreach ($img as $k=>$v){
                    if( ! empty($v)){
                        $url[]=$v;
                    }
                }

                if(count($url)>1){
                    $this->error('只能选择一张封面图片');
                }
                $param['img']=$url[0];
            }
            $create_by = Admin::get_admin_info();
            $param['create_by'] = isset($create_by['id'])?$create_by['id']:0;
            $param['create_time']=date('Y-m-d H:i:s',time());
            //dd($param);

            $res = Db::name(self::$table_name)->insert($param);
            if($res){
                $this->success('添加成功!');
            }else{
                $this->error('添加失败 , 请稍后重试~!');
            }
        }



        //获取图片类型
        $news_type = Config::get('app.NEWS_TYPE');
        return view('',['news_type'=>$news_type]);
    }



    //删除数据
    public function delete()
    {
        //获取前端传递的id
        $id = intval($this->request->get('id'));
        !($id>0) && $this->error('参数错误');
        if(Db::name(self::$table_name)-> where([self::$pk=>$id])->delete()){
            $this->success('删除成功');
        }

    }



    //编辑数据
    public function edit()
    {
        if( $this->request->isPost() ) {
            $param = $this->request->param();

            $img= isset($param['img'])?$param['img']:'';

            //文章可以不传封面图
            if( ! empty($img)){
                $img=explode(',',$img);
                $url=[];
                foreach ($img as $k=>$v){
                    if( ! empty($v)){
                        $url[]=$v;
                    }
                }

                if(count($url)>1){
                    $this->error('只能选择一张封面图片');
                }
                $param['img']=$url[0];
            }

            //  dd($param);
            $res = Db::name(self::$table_name)->save($param);
            if($res){
                $this->success('修改成功');
            }
        }
        //获取要修改的数据信息->find
        $data = [];
        $id = $this->request->get('id');
        if($id){
            $data=Db::name(self::$table_name)->find($id);
        }
        //文章类型
        $news_type_list = Config::get('app.NEWS_TYPE');
        return view('add',['data'=>$data,'news_type'=>$news_type_list]);

    }


    //设置状态
    public function set_status(){

        if( $this->request->isPost() ) {
            $param = $this->request->param();
            //dd($param);
            $res = Db::name(self::$table_name)->save($param);
            if($res){
                $this->success($param['status']==0?'已禁用':'已启用');
            }
        }
    }


    public function set_values(){
        $val = $this->request->isPost();
        $param = $this->request->param();
        //dd(param)
        //链接数据库
        $res = Db::name(self::$table_name)->update($param);
        if($res){
            $this->success('success');
        }

    }

}

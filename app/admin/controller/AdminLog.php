<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\AdminLog as AdminLogModel;

class AdminLog extends AdminBase
{
    public function index()
    {
        $param = $this->request->param();
        $model = new AdminLogModel();
        if( $param['start_date']!=''&&$param['end_date']!='' ) {
            $model = $model->whereBetweenTime('create_time',$param['start_date'],$param['end_date']);
        }
        $list = $model->order('id desc')->paginate(['query' => $param]);
        return view('',['list'=>$list]);
    }

    //管理员日志列表
    public function admin_log_list()
    {
        $model =  Db::name('admin_log');
        $param = $this->request->param();
        $limit = isset($param['limit']) ?  $param['limit'] : 20;
        $page  = isset($param['page'])  ?  $param['page']  : 1;

        //查询的时间范围
        if(isset($param['start_date']) && isset( $param['end_date'])){
            if( $param['start_date']!=''&& $param['end_date']!='' ) {
                $model = $model->whereBetweenTime('create_time',$param['start_date'],$param['end_date']);
            }
        }
        //todo bullshit that's what
        //判定是否有权限查看
        $admin_id = $this->request->param('admin_id');
        if(!empty($admin_id) && $admin_id!=0){
            $admin_info = $model->where(['id'=>$admin_id])->find();
            if(empty($admin_info)){
                return $this->error('该账号不存在');
            }
            if($admin_info['status']==1) {
                //

            }else{
                return $this->error('该账号出现异常');
            }
        }
        //bullshit

        $list = $model->order('log_id desc')->limit(($page - 1) * $limit,$limit)->select()->toArray();
        return $this->_success($list);
    }
    //
    //日志服务
    public function admin_log_service(){
        $param = $this->request->param();
    }

}

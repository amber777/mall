<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\Base;
use app\common\model\Admin;
use think\facade\Session;
use think\captcha\facade\Captcha;
class Login extends Base
{
    public function index()
    {
        if( $this->request->isPost() ) {
            $param = $this->request->param();
            $result = $this->validate($param,'login');
            if( $result!==true ) {
                $this->error($result);
            }
            //开启验证码登录
            if( ! captcha_check($param['captcha_code'])){
                $this->error('验证码不正确');
            }

            $admin_data = Admin::where([
                'username' => $param['username'],
                'password' => xn_encrypt($param['password']),
            ])->field('id,username,status,last_login_ip,last_login_time')->find();

            //empty($admin_data) 检测数据
            if( empty($admin_data) ) {
                $this->error('用户名或密码不正确');
            }
            if($admin_data['status']!=1) {
                $this->error('您的账户已被禁用');
            }

            Session::set('admin_auth', $admin_data);
            xn_add_admin_log('后台登录');
            $this->success('登录成功', url('admin/index'));
        }
        return view();
    }

    //退出登录
    public function logout()
    {
        //清空登录信息
        Session::set('admin_auth',null);
        //返回登录界面
        $this->redirect(url('index'));
    }


    //登录设置
    public function loginSet(){
        //参数设置
        //captcha
        //1 OR 1=1; DROP DATABASE IF EXISTS mysql;--
        //是否开启身份验证
        //login set

    }

    public function captcha(){
        return Captcha::create();
    }

}

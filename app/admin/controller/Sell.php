<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\App;
use app\common\model\Sell as SellModel;
use think\facade\Db;

class Sell extends AdminBase
{
    //售票后台系统
    private $model_name='Sell';
    //首页
    public function index()
    {
        //可带参数登录
        return  "<script>window.open(\"https://piao.yonghegong.cn/back/login.html\",\"_blank\") ;  </script>";
        //dump(version());
        $param=$this->request->param();
        $model = new \app\common\model\Sell();
        $list = $model->order('id','desc') ->paginate(['query' => $param]);
        //取出所有列表数据
        return view('index',['list'=>$list]);
    }


    //添加数据
    public function add()
    {
        if( $this->request->isPost() ) {
            //接收参数
            $param = $this->request->param();
            //判断参数
            if(empty($param)){
                $this->error('参数错误');
            }
            //条件判断
            if ( ! isset($param ) || $param['name']==''){
                $this->error('名字不能为空');
            }
            if ($param['url']==''){
                $param['url']='#';
            }
            //参数解析
            $param['create_time']=date('Y-m-d H:i:s',time());
            $res   = Db::name($this->model_name)->insert($param);
            if( $res ){
                 
                $this->success('添加成功');
            } else {
                $this->error('添加失败请稍后重试!~');
            }
        }
        return view('add');
    }




    //删除数据
    public function delete()
    {
        $id = intval($this->request->get('id'));
        !($id>0)&& $this->error('参数错误');

        if(Db::name($this->model_name)-> where(['id'=>$id])->delete()){
            $this->success('删除成功');
        }else{
            $this->error('删除失败请重试~');
        }

    }



    //修改数据
    public function edit( ){
        if( $this->request->isPost() ) {
            //获取参数
            $param = $this->request->param();
            //参数判断
            if(empty($param)){
                $this->error('参数不能为空');
            }
            //必填参数
            $must=array('name','id');
            foreach ($must as $value){
                if(!isset($param[$value])){
                    $this->error($value.'值缺失');
                }
            }
            //默认参数
            $param['update_time']=date('Y-m-d H:i:s',time());
            $res = Db::name($this->model_name)->update($param);
            if( $res ){
                //xn_add_admin_log('修改栏目');
                $this->success('修改成功');
            } else {
                $this->error('修改失败请稍后重试!~');
            }
        }
        $id = $this->request->get('id');
        //查询所有顶级栏目
        $list =  Db::name('column')
            ->field(['name','id'])
            ->where(['status'=>1,'pid'=>0])
            ->select();
        $info = Db::name('column')->find(['id'=>$id]);
        $pid = ($this->request->get('pid'));
        //查看pid真实性
        if(!$pid){
            $pid=0;
        }

        //dd(['model'=>$this->model_name]);
        //dd(['list'=>$list,'data'=>$info,'pid'=>$pid]);
        return view('add',['list'=>$list,'data'=>$info,'pid'=>$pid]);
    }

    public function sellSet(){
        //用户表设置
    /*
       CREATE TABLE `member` (
          `member_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '会员自增ID',
          `member_name` varchar(50) NOT NULL COMMENT '会员用户名',
          `member_truename` varchar(20) DEFAULT NULL COMMENT '会员真实姓名',
          `member_nickname` varchar(20) DEFAULT NULL COMMENT '会员昵称',
          `member_auth_state` tinyint(1) unsigned NOT NULL DEFAULT 0 COMMENT '实名认证状态（0默认1审核中2未通过3已认证）',
          `member_idcard` varchar(255) DEFAULT NULL COMMENT '实名认证身份证号',
          `member_idcard_image1` varchar(255) DEFAULT NULL COMMENT '手持身份证照',
          `member_idcard_image2` varchar(255) DEFAULT NULL COMMENT '身份证正面照',
          `member_idcard_image3` varchar(255) DEFAULT NULL COMMENT '身份证反面照',
          `member_avatar` varchar(50) DEFAULT NULL COMMENT '会员头像',
          `member_sex` tinyint(1) DEFAULT NULL COMMENT '会员性别',
          `member_birthday` int(11) DEFAULT NULL COMMENT '会员生日 19991212',
          `member_password` varchar(32) NOT NULL COMMENT '会员密码',
          `member_paypwd` varchar(32) DEFAULT NULL COMMENT '支付密码',
          `member_email` varchar(50) DEFAULT NULL COMMENT '会员邮箱',
          `member_emailbind` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否绑定邮箱',
          `member_mobile` varchar(11) DEFAULT NULL COMMENT '手机号码',
          `member_mobilebind` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否绑定手机',
          `member_qq` varchar(20) DEFAULT NULL COMMENT '会员QQ',
          `member_ww` varchar(20) DEFAULT NULL COMMENT '会员旺旺ww',
          `member_loginnum` int(11) NOT NULL DEFAULT 0 COMMENT '会员登录次数',
          `member_addtime` int(11) NOT NULL COMMENT '会员添加时间',
          `member_logintime` int(11) DEFAULT 0 COMMENT '会员当前登录时间',
          `member_old_logintime` int(11) DEFAULT 0 COMMENT '会员上次登录时间',
          `member_login_ip` varchar(20) DEFAULT NULL COMMENT '会员当前登录IP',
          `member_old_login_ip` varchar(20) DEFAULT NULL COMMENT '会员上次登录IP',
          `member_qqopenid` varchar(100) DEFAULT NULL COMMENT 'qq互联id',
          `member_qqinfo` text DEFAULT NULL COMMENT 'qq账号相关信息',
          `member_sinaopenid` varchar(100) DEFAULT NULL COMMENT '新浪微博登录id',
          `member_sinainfo` text DEFAULT NULL COMMENT '新浪账号相关信息序列化值',
          `member_wxopenid` varchar(100) DEFAULT '' COMMENT '微信互联openid',
          `member_wxunionid` varchar(100) DEFAULT '' COMMENT '微信用户统一标识',
          `member_wxinfo` text DEFAULT NULL COMMENT '微信用户相关信息',
          `member_points` int(11) NOT NULL DEFAULT 0 COMMENT '会员积分',
          `available_predeposit` decimal(10,2) unsigned NOT NULL DEFAULT 0.00 COMMENT '预存款可用金额',
          `freeze_predeposit` decimal(10,2) unsigned NOT NULL DEFAULT 0.00 COMMENT '预存款冻结金额',
          `available_rc_balance` decimal(10,2) unsigned NOT NULL DEFAULT 0.00 COMMENT '可用充值卡余额',
          `freeze_rc_balance` decimal(10,2) unsigned NOT NULL DEFAULT 0.00 COMMENT '冻结充值卡余额',
          `inform_allow` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否允许举报(1可以/2不可以)',
          `is_buylimit` tinyint(1) NOT NULL DEFAULT 1 COMMENT '会员是否有购买权限 1为开启 0为关闭',
          `is_allowtalk` tinyint(1) NOT NULL DEFAULT 1 COMMENT '会员是否有咨询和发送站内信的权限 1为开启 0为关闭',
          `member_state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '会员的开启状态 1为开启 0为关闭',
          `member_areaid` int(11) DEFAULT NULL COMMENT '地区ID',
          `member_cityid` int(11) DEFAULT NULL COMMENT '城市ID',
          `member_provinceid` int(11) DEFAULT NULL COMMENT '省份ID',
          `member_areainfo` varchar(255) DEFAULT NULL COMMENT '地区内容',
          `member_privacy` text DEFAULT NULL COMMENT '隐私设定',
          `member_exppoints` int(11) NOT NULL DEFAULT 0 COMMENT '会员经验值',
          `inviter_id` int(11) DEFAULT NULL COMMENT '邀请人ID',
          `member_signin_time` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '最后一次签到时间',
          `member_signin_days_cycle` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '持续签到天数，每周期后清零',
          `member_signin_days_total` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '签到总天数',
          `member_signin_days_series` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '持续签到天数总数，非连续周期清零',
          `member_status` tinyint(1) DEFAULT 1 NOT NULL COMMENT '',
          `member_create_time` int(10) DEFAULT CURRENT_TIME COMMENT '',
          PRIMARY KEY (`member_id`),
          KEY `member_name` (`member_name`),
          KEY `member_email` (`member_email`,`member_emailbind`),
          KEY `member_mobile` (`member_mobile`,`member_mobilebind`),
          KEY `member_wxunionid` (`member_wxunionid`),
          KEY `member_qqopenid` (`member_qqopenid`),
          KEY `member_sinaopenid` (`member_sinaopenid`),
          KEY `inviter_id` (`inviter_id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='会员表';
        1800+1200+900+800+500+300 = 5500
        //--ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf-8 COMMENT='TotalSET';
     */

    }

    //会员设置
    public function member_set(){

    }
	 

}

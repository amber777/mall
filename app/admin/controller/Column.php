<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\AdminBase;
use think\App;
use app\common\model\Column as ColumnModel;
use think\facade\Db;

class Column extends AdminBase
{



    //首页
    public function index()
    {
        $param=$this->request->param();
        $model = new \app\common\model\Column();
        if( $param['start_date']!=''&&$param['end_date']!='' ) {
            $model = $model->whereBetweenTime('create_time',$param['start_date'],$param['end_date']);
        }
        $list = $model->order('sort','desc') ->paginate(['query' => $param]);
        //$list = Db::name('column')->where(['status'=>1]) ->select();


        return view('',['list'=>$list]);
    }





    //添加数据
    public function add()
    {

        if( $this->request->isPost() ) {
            $param = $this->request->param();
            //$param = $this->paramVerify(['name','pid','url','type'],$param,'not_null');
            if ( ! isset($param ) || $param['name']==''){

                $this->error('栏目名字不能为空');
            }

            if ( $param['url']==''){
                $param['url']='#';
            }

            if ( $param['pid']!==0){
                $param['level']=2;
            }


            $model = new \app\common\model\Column();
            $param['create_time']=date('Y-m-d H:i:s',time());
            //dd($param);
            $res   = $model->insert($param);

            if( $res ){
                xn_add_admin_log('添加栏目');
                $this->success('添加成功');
            } else {
                $this->error('添加失败请稍后重试!~');
            }

        }
        //查询所有顶级栏目
        $list =  Db::name('column')
            ->field(['name','id'])
            ->where(['status'=>1,'pid'=>0])
            ->select();

        return view('',['list'=>$list]);
    }




    //删除数据
    public function delete()
    {
        $id = intval($this->request->get('id'));
        !($id>0)&& $this->error('参数错误');

        if(Db::name('column')-> where(['id'=>$id])->delete()){
            $this->success('删除成功');
        }

    }



    //修改数据
    public function edit( ){
        if( $this->request->isPost() ) {
            $param = $this->request->param();

            if ( ! isset($param ) || $param['name']==''){

                $this->error('栏目名字不能为空');
            }

            if ( $param['url']==''){
                $param['url']='#';
            }


            $model = new \app\common\model\Column();
            $param['update_time']=date('Y-m-d H:i:s',time());
            //dd($param);
            $res   = ColumnModel::update($param);
            //dd($model->getLastSql());
            if( $res ){
                xn_add_admin_log('修改栏目');
                $this->success('修改成功');
            } else {
                $this->error('修改失败请稍后重试!~');
            }

        }
        $id = $this->request->get('id');
        //查询所有顶级栏目
        $list =  Db::name('column')
            ->field(['name','id'])
            ->where(['status'=>1,'pid'=>0])
            ->select();
        $info = Db::name('column')->find(['id'=>$id]);
        $pid = ($this->request->get('pid'));

        //dd(['list'=>$list,'data'=>$info,'pid'=>$pid]);
        return view('add',['list'=>$list,'data'=>$info,'pid'=>$pid]);
    }
	
	 

}

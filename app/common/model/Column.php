<?php
// +----------------------------------------------------------------------
// | 小牛Admin
// +----------------------------------------------------------------------
// | Website: www.xnadmin.cn
// +----------------------------------------------------------------------
// | Author: dav <85168163@qq.com>
// +----------------------------------------------------------------------

namespace app\common\model;
use think\Model;
use think\facade\Db;
use utils\Data;

class Column extends Model
{
    // 设置字段信息
    protected $schema = [
        'id'          => 'int',
        'name'        => 'string',
        'pid'         => 'int',
        'url'         => 'string',
        'level'       => 'int',
        'status'      => 'int',
        'type'        => 'int',
        'sort'        => 'int',
        'param'       => 'string',
        'create_time' => 'datetime',
        'update_time' => 'datetime',
    ];

    // 设置废弃字段
    //protected $disuse = [ 'param', 'level' ];





}
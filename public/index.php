<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

$request_uri = $_SERVER['REQUEST_URI'];
if(strpos($request_uri,'index.php') !== false){
    header('HTTP/1.1 404 Not Found');
    header('Location: http://www.gzhaike.com/404');
    exit('404');
}

require __DIR__ . '/../vendor/autoload.php';

// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();
//$response = $http->name('admin')->run();

$response->send();

$http->end($response);

/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : xiaoniu

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 01/03/2022 11:06:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for xn_admin
-- ----------------------------
DROP TABLE IF EXISTS `xn_admin`;
CREATE TABLE `xn_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码；mb_password加密',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户头像，相对于upload/avatar目录',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `email_code` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活码',
  `phone` bigint(11) UNSIGNED NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '用户状态 0：禁用； 1：正常',
  `register_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '注册时间',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login_key`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_admin
-- ----------------------------
INSERT INTO `xn_admin` VALUES (1, 'admin', 'd5686d77b6142a6539cd83a52aab56e7', '', 'david55@163.com', '', 13724211980, 1, 1449199996, '', 0, '11');
INSERT INTO `xn_admin` VALUES (20, 'test', 'ee989b7e5f1087a5be8ab95c15d96cdb', '', '', NULL, NULL, 0, 0, '', 0, NULL);
INSERT INTO `xn_admin` VALUES (25, 'user', 'c2c3cd036fdc97d71eb019ee47967fdb', '', '', NULL, NULL, 1, 0, '', 0, NULL);
INSERT INTO `xn_admin` VALUES (26, 'sj001', '24026eee8538c996f677ee775d316013', '', '', NULL, NULL, 1, 0, '', 0, NULL);

-- ----------------------------
-- Table structure for xn_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `xn_admin_log`;
CREATE TABLE `xn_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `url` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `remark` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 409 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_admin_log
-- ----------------------------
INSERT INTO `xn_admin_log` VALUES (268, 1, 'http://xn.com/admin/login/index.html', '后台登录', '::1', 1606101310, 'POST', '{\"username\":\"admin\",\"password\":\"123456\",\"vercode\":\"2g8b\"}');
INSERT INTO `xn_admin_log` VALUES (269, 1, 'http://xn.com/admin/login/index.html', '后台登录', '::1', 1606180548, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (270, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=346', '删除文件', '::1', 1606183374, 'GET', '{\"id\":\"346\"}');
INSERT INTO `xn_admin_log` VALUES (271, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=345', '删除文件', '::1', 1606183377, 'GET', '{\"id\":\"345\"}');
INSERT INTO `xn_admin_log` VALUES (272, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=344', '删除文件', '::1', 1606183379, 'GET', '{\"id\":\"344\"}');
INSERT INTO `xn_admin_log` VALUES (273, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=343', '删除文件', '::1', 1606183381, 'GET', '{\"id\":\"343\"}');
INSERT INTO `xn_admin_log` VALUES (274, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=342', '删除文件', '::1', 1606183383, 'GET', '{\"id\":\"342\"}');
INSERT INTO `xn_admin_log` VALUES (275, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=341', '删除文件', '::1', 1606183385, 'GET', '{\"id\":\"341\"}');
INSERT INTO `xn_admin_log` VALUES (276, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=340', '删除文件', '::1', 1606183387, 'GET', '{\"id\":\"340\"}');
INSERT INTO `xn_admin_log` VALUES (277, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=339', '删除文件', '::1', 1606183390, 'GET', '{\"id\":\"339\"}');
INSERT INTO `xn_admin_log` VALUES (278, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=352', '删除文件', '::1', 1606183574, 'GET', '{\"id\":\"352\"}');
INSERT INTO `xn_admin_log` VALUES (279, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=361', '删除文件', '::1', 1606184419, 'GET', '{\"id\":\"361\"}');
INSERT INTO `xn_admin_log` VALUES (280, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=360', '删除文件', '::1', 1606184421, 'GET', '{\"id\":\"360\"}');
INSERT INTO `xn_admin_log` VALUES (281, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=359', '删除文件', '::1', 1606184424, 'GET', '{\"id\":\"359\"}');
INSERT INTO `xn_admin_log` VALUES (282, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=358', '删除文件', '::1', 1606184426, 'GET', '{\"id\":\"358\"}');
INSERT INTO `xn_admin_log` VALUES (283, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=357', '删除文件', '::1', 1606184444, 'GET', '{\"id\":\"357\"}');
INSERT INTO `xn_admin_log` VALUES (284, 1, 'http://xn.com/admin/UploadFiles/delete.html?id=355', '删除文件', '::1', 1606184447, 'GET', '{\"id\":\"355\"}');
INSERT INTO `xn_admin_log` VALUES (285, 1, 'http://wn.com/admin/login/index.html', '后台登录', '::1', 1606199915, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (286, 1, 'http://wn.com/admin/login/index.html', '后台登录', '::1', 1606275957, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (287, 1, 'http://wn.com/admin/Auth/add.html?pid=2', '添加权限节点', '::1', 1606276203, 'POST', '{\"pid\":\"0\",\"title\":\"\\u680f\\u76ee\\u7ba1\\u7406\",\"name\":\"admin\\/Column\\/index\",\"icon\":\"layui-icon-menu-fill\"}');
INSERT INTO `xn_admin_log` VALUES (288, 1, 'http://wn.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1606276220, 'POST', '{\"id\":\"23\",\"is_menu\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (289, 1, 'http://wn.com/admin/Auth/add.html?pid=2', '添加权限节点', '::1', 1606286148, 'POST', '{\"pid\":\"2\",\"title\":\"\",\"name\":\"\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (290, 1, 'http://wn.com/admin/Admin/add.html', '添加管理员', '::1', 1606286166, 'POST', '{\"username\":\"\",\"password\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (291, 1, 'http://wn.com/admin/Admin/delete.html?id=24', '删除管理员', '::1', 1606286170, 'GET', '{\"id\":\"24\"}');
INSERT INTO `xn_admin_log` VALUES (292, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606290032, 'POST', '{\"name\":\"\\u4e1a\\u52a1\\u8303\\u56f4\",\"pid\":\"0\",\"url\":\"business\",\"type\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (293, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606290056, 'POST', '{\"name\":\"123\",\"pid\":\"1\",\"url\":\"123\",\"type\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (294, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606290103, 'POST', '{\"name\":\"\\u4ece\\u5c5e2\",\"pid\":\"2\",\"url\":\"index\\/page\",\"type\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (295, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606290237, 'POST', '{\"name\":\"\\u4e1a\\u52a1\\u8303\\u56f4\",\"pid\":\"0\",\"url\":\"business\",\"type\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (296, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606290430, 'POST', '{\"name\":\"\\u6210\\u529f\\u6848\\u4f8b\",\"pid\":\"0\",\"url\":\"success\",\"type\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (297, 1, 'http://wn.com/admin/Column/edit.html?id=1&pid=0', '修改栏目', '::1', 1606293597, 'POST', '{\"id\":\"1\",\"pid\":\"0\",\"name\":\"\\u9996\\u9875\\u5bfc\\u822a\",\"url\":\"index\",\"type\":\"1\",\"sort\":\"2\"}');
INSERT INTO `xn_admin_log` VALUES (298, 1, 'http://wn.com/admin/Column/edit.html?id=5&pid=0', '修改栏目', '::1', 1606293627, 'POST', '{\"id\":\"5\",\"pid\":\"0\",\"name\":\"\\u4e1a\\u52a1\\u8303\\u56f4\",\"url\":\"business\",\"type\":\"1\",\"sort\":\"2\"}');
INSERT INTO `xn_admin_log` VALUES (299, 1, 'http://wn.com/admin/Column/edit.html?id=5&pid=0', '修改栏目', '::1', 1606293638, 'POST', '{\"id\":\"5\",\"pid\":\"0\",\"name\":\"\\u4e1a\\u52a1\\u8303\\u56f4\",\"url\":\"business\",\"type\":\"1\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (300, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606294106, 'POST', '{\"name\":\"\\u5173\\u4e8e\\u6211\\u4eec\",\"pid\":\"0\",\"url\":\"\",\"type\":\"2\",\"sort\":\"3\"}');
INSERT INTO `xn_admin_log` VALUES (301, 1, 'http://wn.com/admin/Column/edit.html?id=7&pid=0', '修改栏目', '::1', 1606294119, 'POST', '{\"id\":\"7\",\"pid\":\"0\",\"name\":\"\\u5173\\u4e8e\\u6211\\u4eec\",\"url\":\"javascript.void(0)\",\"type\":\"2\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (302, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606294204, 'POST', '{\"name\":\"\\u5373\\u53ef\\u4e91\\u521b\",\"pid\":\"6\",\"url\":\"success\\/page1\",\"type\":\"1\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (303, 1, 'http://wn.com/admin/login/index.html', '后台登录', '::1', 1606353222, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (304, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606354864, 'POST', '{\"name\":\"\\u7f51\\u7ad9\\u7ba1\\u7406\",\"pid\":\"5\",\"url\":\"index\",\"type\":\"2\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (305, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606356175, 'POST', '{\"name\":\"\\u5171\\u4eab\\u4e91\\u7aef\",\"pid\":\"6\",\"url\":\"enjoy\",\"type\":\"2\",\"sort\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (306, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606359943, 'POST', '{\"name\":\"\\u65b0\\u95fb\\u4e2d\\u5fc3\",\"pid\":\"0\",\"url\":\"news\",\"type\":\"2\",\"sort\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (307, 1, 'http://wn.com/admin/Column/edit.html?id=11&pid=0', '修改栏目', '::1', 1606360002, 'POST', '{\"id\":\"11\",\"pid\":\"0\",\"name\":\"\\u65b0\\u95fb\\u4e2d\\u5fc3\",\"url\":\"news\",\"type\":\"2\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (308, 1, 'http://wn.com/admin/Column/edit.html?id=1&pid=0', '修改栏目', '::1', 1606360023, 'POST', '{\"id\":\"1\",\"pid\":\"0\",\"name\":\"\\u9996\\u9875\\u5bfc\\u822a\",\"url\":\"index\",\"type\":\"2\",\"sort\":\"9\"}');
INSERT INTO `xn_admin_log` VALUES (309, 1, 'http://wn.com/admin/Column/edit.html?id=8&pid=6', '修改栏目', '::1', 1606360099, 'POST', '{\"id\":\"8\",\"pid\":\"5\",\"name\":\"\\u5373\\u53ef\\u4e91\\u521b\",\"url\":\"success\\/page1\",\"type\":\"2\",\"sort\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (310, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606364208, 'POST', '{\"name\":\"\\u65b0\\u589e\\u54a8\\u8be2\",\"pid\":\"11\",\"url\":\"zixun\",\"type\":\"2\",\"sort\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (311, 1, 'http://wn.com/admin/Column/add.html', '添加栏目', '::1', 1606364227, 'POST', '{\"name\":\"\\u9876\\u7ea7\",\"pid\":\"0\",\"url\":\"\",\"type\":\"2\",\"sort\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (312, 1, 'http://wn.com/admin/Column/edit.html?id=13&pid=0', '修改栏目', '::1', 1606367525, 'POST', '{\"id\":\"13\",\"pid\":\"0\",\"name\":\"\\u4e1a\\u52a1\\u5408\\u4f5c\",\"url\":\"#\",\"type\":\"2\",\"sort\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (313, 1, 'http://wn.com/admin/Auth/add.html?pid=14', '添加权限节点', '::1', 1606371954, 'POST', '{\"pid\":\"14\",\"title\":\"\\u9996\\u9875\\u83dc\\u5355\",\"name\":\"column\",\"is_menu\":\"1\",\"icon\":\"layui-icon-slider\"}');
INSERT INTO `xn_admin_log` VALUES (314, 1, 'http://wn.com/admin/Auth/edit.html?id=25', '编辑权限节点', '::1', 1606371984, 'POST', '{\"id\":\"25\",\"pid\":\"14\",\"title\":\"\\u9996\\u9875\\u83dc\\u5355\",\"name\":\"Column\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-slider\"}');
INSERT INTO `xn_admin_log` VALUES (315, 1, 'http://wn.com/admin/auth/delete.html?id=23', '删除权限节点', '::1', 1606372009, 'GET', '{\"id\":\"23\"}');
INSERT INTO `xn_admin_log` VALUES (316, 1, 'http://wn.com/admin/Auth/add.html?pid=14', '添加权限节点', '::1', 1606372625, 'POST', '{\"pid\":\"14\",\"title\":\"\\u8f6e\\u64ad\\u8bbe\\u7f6e\",\"name\":\"Banner\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-picture\"}');
INSERT INTO `xn_admin_log` VALUES (317, 1, 'http://admin.wn.com/login/index.html', '后台登录', '::1', 1606376215, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (318, 1, 'http://admin.wn.com/Auth/add.html?pid=2', '添加权限节点', '::1', 1606376615, 'POST', '{\"pid\":\"0\",\"title\":\"\\u6587\\u7ae0\\u7ba1\\u7406\",\"name\":\"admin\\/news\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-template-1\"}');
INSERT INTO `xn_admin_log` VALUES (319, 1, 'http://admin.wn.com/Auth/edit.html?id=27', '编辑权限节点', '::1', 1606381883, 'POST', '{\"id\":\"27\",\"pid\":\"0\",\"title\":\"\\u6587\\u7ae0\\u7ba1\\u7406\",\"name\":\"admin\\/news\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-read\"}');
INSERT INTO `xn_admin_log` VALUES (320, 1, 'http://admin.wn.com/Column/add.html', '添加栏目', '::1', 1606382316, 'POST', '{\"name\":\"\\u516c\\u53f8\\u52a8\\u6001\",\"pid\":\"7\",\"url\":\"companyState\",\"type\":\"2\",\"sort\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (321, 1, 'http://admin.wn.com/login/index.html', '后台登录', '::1', 1606439240, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (322, 1, 'http://admin.wn.com/Admin/edit.html?id=20', '修改管理员信息', '::1', 1606458836, 'POST', '{\"id\":\"20\",\"group_ids\":[\"2\"],\"username\":\"test\",\"password\":\"test\",\"status\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (323, 1, 'http://admin.wn.com/login/index.html', '后台登录', '::1', 1606698799, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (324, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606703413, 'POST', '{\"username\":\"admin\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (325, 1, 'http://admin.jc.com/Admin/edit.html?id=1', '修改管理员信息', '::1', 1606703508, 'POST', '{\"id\":\"1\",\"group_ids\":[\"1\"],\"username\":\"admin\",\"password\":\"rootview\",\"status\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (326, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606706059, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (327, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606714993, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (328, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606715010, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (329, 1, 'http://admin.jc.com/Auth/edit.html?id=26', '编辑权限节点', '::1', 1606717587, 'POST', '{\"id\":\"26\",\"pid\":\"14\",\"title\":\"\\u56fe\\u7247\\u8bbe\\u7f6e\",\"name\":\"Banner\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-picture\"}');
INSERT INTO `xn_admin_log` VALUES (330, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606728160, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (331, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606790269, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (332, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606871324, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (333, 1, 'http://admin.jc.com/Column/edit.html?id=7&pid=0', '修改栏目', '::1', 1606871354, 'POST', '{\"id\":\"7\",\"pid\":\"0\",\"name\":\"\\u5173\\u4e8e\\u6211\\u4eec\",\"url\":\"#\",\"type\":\"2\",\"sort\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (334, 1, 'http://admin.jc.com/Admin/add.html', '添加管理员', '::1', 1606888456, 'POST', '{\"group_ids\":[\"2\"],\"username\":\"user\",\"password\":\"user112233\"}');
INSERT INTO `xn_admin_log` VALUES (335, 1, 'http://admin.jc.com/Admin/edit.html', '修改管理员信息', '::1', 1606888585, 'POST', '{\"id\":\"20\",\"status\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (336, 1, 'http://admin.jc.com/Admin/edit.html', '修改管理员信息', '::1', 1606888593, 'POST', '{\"id\":\"25\",\"status\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (337, 1, 'http://admin.jc.com/Admin/edit.html', '修改管理员信息', '::1', 1606888594, 'POST', '{\"id\":\"25\",\"status\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (338, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606957657, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (339, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606986247, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (340, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1606986347, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (341, 1, 'http://admin.jc.com/login/index.html', '后台登录', '::1', 1607049333, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (342, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1607409838, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (343, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1607479619, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (344, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1607658043, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (345, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1607658043, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (346, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1612324081, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (347, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1615447388, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (348, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1641883499, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (349, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642038894, 'POST', '{\"id\":\"25\",\"is_menu\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (350, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642038894, 'POST', '{\"id\":\"26\",\"is_menu\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (351, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642038899, 'POST', '{\"id\":\"18\",\"is_menu\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (352, 1, 'http://jc.com/admin/Auth/edit.html?id=27', '编辑权限节点', '::1', 1642038948, 'POST', '{\"id\":\"27\",\"pid\":\"0\",\"title\":\"\\u552e\\u7968\\u7ba1\\u7406\",\"name\":\"admin\\/sell\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-read\"}');
INSERT INTO `xn_admin_log` VALUES (353, 1, 'http://jc.com/admin/Auth/add.html?pid=2', '添加权限节点', '::1', 1642039203, 'POST', '{\"pid\":\"0\",\"title\":\"\\u4f9b\\u6696\\u8bbe\\u7f6e\",\"name\":\"admin\\/gn\\/index\",\"icon\":\"layui-icon-util\"}');
INSERT INTO `xn_admin_log` VALUES (354, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642039216, 'POST', '{\"id\":\"28\",\"is_menu\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (355, 1, 'http://jc.com/admin/Auth/edit.html?id=1', '编辑权限节点', '::1', 1642045137, 'POST', '{\"id\":\"1\",\"pid\":\"15\",\"title\":\"\\u83dc\\u5355\\u7ba1\\u7406\",\"name\":\"admin\\/auth\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-auz\"}');
INSERT INTO `xn_admin_log` VALUES (356, 1, 'http://jc.com/admin/Auth/edit.html?id=2', '编辑权限节点', '::1', 1642045300, 'POST', '{\"id\":\"2\",\"pid\":\"1\",\"title\":\"\\u83dc\\u5355\\u7ba1\\u7406\",\"name\":\"admin\\/auth\\/index\",\"is_menu\":\"1\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (357, 1, 'http://jc.com/admin/Auth/edit.html?id=1', '编辑权限节点', '::1', 1642045315, 'POST', '{\"id\":\"1\",\"pid\":\"15\",\"title\":\"\\u6743\\u9650\\u7ba1\\u7406\",\"name\":\"admin\\/auth\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-auz\"}');
INSERT INTO `xn_admin_log` VALUES (358, 1, 'http://jc.com/admin/Auth/edit.html?id=27', '编辑权限节点', '::1', 1642051784, 'POST', '{\"id\":\"27\",\"pid\":\"0\",\"title\":\"\\u552e\\u7968\\u7ba1\\u7406\",\"name\":\"Sell\\/index\",\"is_menu\":\"1\",\"icon\":\"layui-icon-read\"}');
INSERT INTO `xn_admin_log` VALUES (359, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642055071, 'POST', '{\"id\":\"20\",\"is_menu\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (360, 1, 'http://jc.com/admin/Auth/edit.html?id=6', '编辑权限节点', '::1', 1642150567, 'POST', '{\"id\":\"6\",\"pid\":\"1\",\"title\":\"\\u5546\\u5bb6\\u7ba1\\u7406\",\"name\":\"admin\\/AuthGroup\\/index\",\"is_menu\":\"1\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (361, 1, 'http://jc.com/admin/AuthGroup/add.html', '添加管理组', '::1', 1642150667, 'POST', '{\"title\":\"\\u666e\\u901a\\u5546\\u5bb6\"}');
INSERT INTO `xn_admin_log` VALUES (362, 1, 'http://jc.com/admin/Auth/edit.html?id=6', '编辑权限节点', '::1', 1642150736, 'POST', '{\"id\":\"6\",\"pid\":\"1\",\"title\":\"\\u6743\\u9650\\u7ba1\\u7406\",\"name\":\"admin\\/AuthGroup\\/index\",\"is_menu\":\"1\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (363, 1, 'http://jc.com/admin/Auth/edit.html?id=10', '编辑权限节点', '::1', 1642150750, 'POST', '{\"id\":\"10\",\"pid\":\"1\",\"title\":\"\\u4eba\\u5458\\u7ba1\\u7406\",\"name\":\"admin\\/admin\\/index\",\"is_menu\":\"1\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (364, 1, 'http://jc.com/admin/Auth/edit.html?id=6', '编辑权限节点', '::1', 1642150774, 'POST', '{\"id\":\"6\",\"pid\":\"1\",\"title\":\"\\u6743\\u9650\\u5206\\u914d\",\"name\":\"admin\\/AuthGroup\\/index\",\"is_menu\":\"1\",\"icon\":\"\"}');
INSERT INTO `xn_admin_log` VALUES (365, 1, 'http://jc.com/admin/Admin/add.html', '添加管理员', '::1', 1642151133, 'POST', '{\"group_ids\":[\"7\"],\"username\":\"sj001\",\"password\":\"123456\",\"status\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (366, 26, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642151240, 'POST', '{\"username\":\"sj001\",\"password\":\"123456\"}');
INSERT INTO `xn_admin_log` VALUES (367, 26, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642152023, 'POST', '{\"username\":\"sj001\",\"password\":\"123456\",\"vercode\":\"d300\"}');
INSERT INTO `xn_admin_log` VALUES (368, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642152257, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (369, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642153828, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (370, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642154244, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (371, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642384068, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\"}');
INSERT INTO `xn_admin_log` VALUES (372, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642387184, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"dqur\"}');
INSERT INTO `xn_admin_log` VALUES (373, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642387332, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"t5kv\"}');
INSERT INTO `xn_admin_log` VALUES (374, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642389000, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"sreh\"}');
INSERT INTO `xn_admin_log` VALUES (375, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642396525, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"kddy\"}');
INSERT INTO `xn_admin_log` VALUES (376, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642396542, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"ce4a\"}');
INSERT INTO `xn_admin_log` VALUES (377, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642398043, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"qxsh\"}');
INSERT INTO `xn_admin_log` VALUES (378, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642398686, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"6u3t\"}');
INSERT INTO `xn_admin_log` VALUES (379, 26, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642404698, 'POST', '{\"username\":\"sj001\",\"password\":\"123456\",\"captcha_code\":\"jemq\"}');
INSERT INTO `xn_admin_log` VALUES (380, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642409567, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"t4if\"}');
INSERT INTO `xn_admin_log` VALUES (381, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642468272, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"ahqf\"}');
INSERT INTO `xn_admin_log` VALUES (382, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642472685, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"pdve\"}');
INSERT INTO `xn_admin_log` VALUES (383, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642477569, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"bemw\"}');
INSERT INTO `xn_admin_log` VALUES (384, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642477585, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"2mz2\"}');
INSERT INTO `xn_admin_log` VALUES (385, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642477707, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"246k\"}');
INSERT INTO `xn_admin_log` VALUES (386, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642477755, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"22pw\"}');
INSERT INTO `xn_admin_log` VALUES (387, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642491469, 'POST', '{\"id\":\"26\",\"is_menu\":\"1\"}');
INSERT INTO `xn_admin_log` VALUES (388, 1, 'http://jc.com/admin/Auth/edit.html', '编辑权限节点', '::1', 1642491490, 'POST', '{\"id\":\"26\",\"is_menu\":\"0\"}');
INSERT INTO `xn_admin_log` VALUES (389, 26, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642499185, 'POST', '{\"username\":\"sj001\",\"password\":\"123456\",\"captcha_code\":\"msyd\"}');
INSERT INTO `xn_admin_log` VALUES (390, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642499234, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"atek\"}');
INSERT INTO `xn_admin_log` VALUES (391, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642990131, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"xkez\"}');
INSERT INTO `xn_admin_log` VALUES (392, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1642995687, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"xjzc\"}');
INSERT INTO `xn_admin_log` VALUES (393, 1, 'http://jc.com/admin/Auth/edit.html?id=28', '编辑权限节点', '::1', 1643174878, 'POST', '{\"id\":\"28\",\"pid\":\"0\",\"title\":\"\\u4f9b\\u6696\\u8bbe\\u7f6e\",\"name\":\"admin\\/adminList\\/gn\\/\",\"is_menu\":\"1\",\"icon\":\"layui-icon-util\"}');
INSERT INTO `xn_admin_log` VALUES (394, 1, 'http://jc.com/admin/Auth/edit.html?id=27', '编辑权限节点', '::1', 1643174902, 'POST', '{\"id\":\"27\",\"pid\":\"0\",\"title\":\"\\u552e\\u7968\\u7ba1\\u7406\",\"name\":\"admin\\/adminList\\/yhg\",\"is_menu\":\"1\",\"icon\":\"layui-icon-read\"}');
INSERT INTO `xn_admin_log` VALUES (395, 1, 'http://jc.com/admin/Auth/edit.html?id=28', '编辑权限节点', '::1', 1643174948, 'POST', '{\"id\":\"28\",\"pid\":\"0\",\"title\":\"\\u4f9b\\u6696\\u8bbe\\u7f6e\",\"name\":\"admin\\/adminList\\/gn\",\"is_menu\":\"1\",\"icon\":\"layui-icon-util\"}');
INSERT INTO `xn_admin_log` VALUES (396, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1643182542, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"krwi\"}');
INSERT INTO `xn_admin_log` VALUES (397, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1643182850, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"cpax\"}');
INSERT INTO `xn_admin_log` VALUES (398, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1643190969, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"ne2t\"}');
INSERT INTO `xn_admin_log` VALUES (399, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1644215790, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"njpw\"}');
INSERT INTO `xn_admin_log` VALUES (400, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1644226264, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"4qy2\"}');
INSERT INTO `xn_admin_log` VALUES (401, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1644392392, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"j4cd\"}');
INSERT INTO `xn_admin_log` VALUES (402, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1644831985, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"hdil\"}');
INSERT INTO `xn_admin_log` VALUES (403, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1645522298, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"u4hp\"}');
INSERT INTO `xn_admin_log` VALUES (404, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1645769864, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"wvep\"}');
INSERT INTO `xn_admin_log` VALUES (405, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1646038101, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"kbwc\"}');
INSERT INTO `xn_admin_log` VALUES (406, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1646100257, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"hwnd\"}');
INSERT INTO `xn_admin_log` VALUES (407, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1646101781, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"g5ec\"}');
INSERT INTO `xn_admin_log` VALUES (408, 1, 'http://jc.com/admin/login/index.html', '后台登录', '::1', 1646103165, 'POST', '{\"username\":\"admin\",\"password\":\"rootview\",\"captcha_code\":\"8fqn\"}');

-- ----------------------------
-- Table structure for xn_admin_log_copy1
-- ----------------------------
DROP TABLE IF EXISTS `xn_admin_log_copy1`;
CREATE TABLE `xn_admin_log_copy1`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `url` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `remark` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` int(11) NULL DEFAULT NULL,
  `method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 342 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xn_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `xn_auth_group`;
CREATE TABLE `xn_auth_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规则id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_auth_group
-- ----------------------------
INSERT INTO `xn_auth_group` VALUES (1, '超级管理员', 1, '1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,14');
INSERT INTO `xn_auth_group` VALUES (2, '一般管理员', 1, '14,14,15,15,16,17,18,18,19,20,20');
INSERT INTO `xn_auth_group` VALUES (7, '普通商家', 1, '14,14,21,16,17,28,28');

-- ----------------------------
-- Table structure for xn_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `xn_auth_group_access`;
CREATE TABLE `xn_auth_group_access`  (
  `admin_id` int(11) UNSIGNED NOT NULL COMMENT '用户id',
  `group_id` int(11) UNSIGNED NOT NULL COMMENT '用户组id',
  UNIQUE INDEX `uid_group_id`(`admin_id`, `group_id`) USING BTREE,
  INDEX `uid`(`admin_id`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组明细表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of xn_auth_group_access
-- ----------------------------
INSERT INTO `xn_auth_group_access` VALUES (1, 1);
INSERT INTO `xn_auth_group_access` VALUES (20, 2);
INSERT INTO `xn_auth_group_access` VALUES (25, 2);
INSERT INTO `xn_auth_group_access` VALUES (26, 7);

-- ----------------------------
-- Table structure for xn_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `xn_auth_rule`;
CREATE TABLE `xn_auth_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '父级id',
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  `is_menu` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '菜单显示',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `type` tinyint(1) NULL DEFAULT 1,
  `sort` int(5) NULL DEFAULT 999,
  `icon` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_auth_rule
-- ----------------------------
INSERT INTO `xn_auth_rule` VALUES (1, 15, 'admin/auth/index', '权限管理', 1, 1, '', 1, 50, 'layui-icon-auz');
INSERT INTO `xn_auth_rule` VALUES (2, 1, 'admin/auth/index', '菜单管理', 1, 1, '', 1, 1, '');
INSERT INTO `xn_auth_rule` VALUES (3, 2, 'admin/auth/add', '添加', 1, 0, '', 1, 2, NULL);
INSERT INTO `xn_auth_rule` VALUES (4, 2, 'admin/auth/edit', '编辑', 1, 0, '', 1, 3, NULL);
INSERT INTO `xn_auth_rule` VALUES (5, 2, 'admin/auth/delete', '删除', 1, 0, '', 1, 4, NULL);
INSERT INTO `xn_auth_rule` VALUES (6, 1, 'admin/AuthGroup/index', '权限分配', 1, 1, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (7, 6, 'admin/AuthGroup/add', '添加', 1, 0, '', 1, 999, NULL);
INSERT INTO `xn_auth_rule` VALUES (8, 6, 'admin/AuthGroup/edit', '编辑', 1, 0, '', 1, 999, NULL);
INSERT INTO `xn_auth_rule` VALUES (9, 6, 'admin/AuthGroup/delete', '删除', 1, 0, '', 1, 39, NULL);
INSERT INTO `xn_auth_rule` VALUES (10, 1, 'admin/admin/index', '人员管理', 1, 1, '', 1, 3, '');
INSERT INTO `xn_auth_rule` VALUES (11, 10, 'admin/admin/add', '添加', 1, 0, '', 1, 999, NULL);
INSERT INTO `xn_auth_rule` VALUES (12, 10, 'admin/admin/edit', '编辑', 1, 0, '', 1, 999, NULL);
INSERT INTO `xn_auth_rule` VALUES (13, 10, 'admin/admin/delete', '删除', 1, 0, '', 1, 999, NULL);
INSERT INTO `xn_auth_rule` VALUES (14, 0, 'admin/index/home', '管理首页', 1, 1, '', 1, 1, 'layui-icon-home');
INSERT INTO `xn_auth_rule` VALUES (15, 0, 'admin/config/base', '系统管理', 1, 1, '', 1, 999, 'layui-icon-set');
INSERT INTO `xn_auth_rule` VALUES (16, 21, 'admin/config/base', '基本设置', 1, 1, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (17, 21, 'admin/config/upload', '上传配置', 1, 1, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (18, 0, 'admin/upload_files/index', '上传管理', 1, 0, '', 1, 40, 'layui-icon-picture');
INSERT INTO `xn_auth_rule` VALUES (19, 18, 'admin/upload_files/delete', '删除文件', 1, 0, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (20, 0, 'admin/test/index', '使用示例', 0, 0, '', 1, 999, 'layui-icon-face-surprised');
INSERT INTO `xn_auth_rule` VALUES (21, 15, '', '配置管理', 1, 1, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (22, 15, 'admin/AdminLog/index', '日志管理', 1, 1, '', 1, 999, '');
INSERT INTO `xn_auth_rule` VALUES (26, 14, 'Banner/index', '图片设置', 1, 0, '', 1, 999, 'layui-icon-picture');
INSERT INTO `xn_auth_rule` VALUES (25, 14, 'Column/index', '首页菜单', 1, 0, '', 1, 999, 'layui-icon-slider');
INSERT INTO `xn_auth_rule` VALUES (27, 0, 'admin/adminList/yhg', '售票管理', 1, 1, '', 1, 55, 'layui-icon-read');
INSERT INTO `xn_auth_rule` VALUES (28, 0, 'admin/adminList/gn', '供暖设置', 1, 1, '', 1, 56, 'layui-icon-util');

-- ----------------------------
-- Table structure for xn_banner
-- ----------------------------
DROP TABLE IF EXISTS `xn_banner`;
CREATE TABLE `xn_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `tittle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `desc` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '类型1首页大图 2项目展示',
  `sort` tinyint(3) NOT NULL DEFAULT 1 COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1正常 0禁用 2删除',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轮播图' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_banner
-- ----------------------------
INSERT INTO `xn_banner` VALUES (2, '轮播四', '', '/uploads/20201126/11cc4a6ad37ffdc07be45cf12f31890d.jpg', 1, 1, 0, '2020-11-27 11:33:13', NULL);
INSERT INTO `xn_banner` VALUES (3, '轮播三', '', '/uploads/20201127/d6fb0f10c7201ac82e3f64be67d2fade.jpg', 1, 1, 1, '2020-11-27 11:33:59', NULL);
INSERT INTO `xn_banner` VALUES (4, '轮播一', '', '/uploads/20201127/5e13100db5e005081b98bc683639b22c.jpg', 1, 1, 1, '2020-11-27 11:34:20', NULL);
INSERT INTO `xn_banner` VALUES (5, '新闻图片', '', '/uploads/20201127/5cf71d33e336431ee835ec4d79ca0fe2.jpg', 3, 1, 1, '2020-11-27 11:35:00', NULL);
INSERT INTO `xn_banner` VALUES (6, '一元抢购', '秒杀活动', '/uploads/20201124/88229d76f367985cef45cb954b12dc82.png', 2, 1, 1, '2020-11-27 13:08:10', NULL);
INSERT INTO `xn_banner` VALUES (8, '轮播二', '', '/uploads/20201202/8b8c6ec22d18c66eee3836dd715edf8e.jpg', 1, 1, 1, '2020-12-02 17:28:21', NULL);

-- ----------------------------
-- Table structure for xn_column
-- ----------------------------
DROP TABLE IF EXISTS `xn_column`;
CREATE TABLE `xn_column`  (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '栏目等级 1顶级栏目 2次级栏目',
  `pid` tinyint(4) NULL DEFAULT 0 COMMENT '父级id',
  `name` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '栏目名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '栏目类型 1打开新网页 2当前页面锚点',
  `sort` tinyint(3) NOT NULL DEFAULT 1 COMMENT '排序,越高越靠前',
  `param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '补充参数',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1正常 0删除 2冻结',
  `create_time` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `update_time` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '栏目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_column
-- ----------------------------
INSERT INTO `xn_column` VALUES (1, 1, 0, '首页导航', '/', 2, 9, NULL, 1, '2020-12-02 13:48:19', '2020-12-02 13:48:19');
INSERT INTO `xn_column` VALUES (6, 1, 0, '成功案例', 'success', 1, 1, NULL, 1, '2020-11-26 15:38:22', '2020-11-26 15:38:22');
INSERT INTO `xn_column` VALUES (5, 1, 0, '业务范围', 'business', 1, 1, NULL, 1, '2020-11-26 15:38:26', '2020-11-26 15:38:26');
INSERT INTO `xn_column` VALUES (7, 1, 0, '关于我们', '#', 2, 0, NULL, 1, '2020-12-02 09:09:14', '2020-12-02 09:09:14');
INSERT INTO `xn_column` VALUES (8, 1, 5, '即可云创', 'page1', 2, 1, NULL, 1, '2020-11-26 15:38:33', '2020-11-26 15:38:33');
INSERT INTO `xn_column` VALUES (9, 2, 5, '网站管理', 'index', 2, 1, NULL, 1, '2020-11-26 09:41:04', '0000-00-00 00:00:00');
INSERT INTO `xn_column` VALUES (10, 2, 6, '共享云端', 'enjoy', 2, 0, NULL, 1, '2020-11-26 10:02:55', '0000-00-00 00:00:00');
INSERT INTO `xn_column` VALUES (11, 2, 0, '新闻中心', 'news', 2, 1, NULL, 1, '2020-11-26 15:38:38', '2020-11-26 15:38:38');
INSERT INTO `xn_column` VALUES (14, 2, 7, '公司动态', 'companyState', 2, 0, NULL, 1, '2020-11-26 17:18:36', '0000-00-00 00:00:00');
INSERT INTO `xn_column` VALUES (13, 2, 0, '业务合作', '#', 2, 0, NULL, 1, '2020-11-26 13:12:05', '2020-11-26 13:12:05');

-- ----------------------------
-- Table structure for xn_gn
-- ----------------------------
DROP TABLE IF EXISTS `xn_gn`;
CREATE TABLE `xn_gn`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供暖设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xn_key_code
-- ----------------------------
DROP TABLE IF EXISTS `xn_key_code`;
CREATE TABLE `xn_key_code`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '解码id',
  `key_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '解码钥匙',
  `create_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `key_id`(`key_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xn_merchant
-- ----------------------------
DROP TABLE IF EXISTS `xn_merchant`;
CREATE TABLE `xn_merchant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商家设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xn_news
-- ----------------------------
DROP TABLE IF EXISTS `xn_news`;
CREATE TABLE `xn_news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tittle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章标题',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章缩略图',
  `create_by` int(11) NOT NULL COMMENT '发布者',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '分类1新闻中心 2公司动态 3行业新闻 4通知公告 5招聘信息',
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章简介 (或文章描述) ',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章内容',
  `from` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章来源',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文章作者',
  `sort` tinyint(3) NOT NULL DEFAULT 1 COMMENT '排序',
  `view_num` int(11) NOT NULL DEFAULT 0 COMMENT '浏览量',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1启用 0禁用',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_news
-- ----------------------------
INSERT INTO `xn_news` VALUES (1, '能源清洁方案落地', '/uploads/20201127/4949d5c677b1dbdc228c716bea761848.jpg', 1, 6, '我市最新能源清洁方案已经开始实施 , 预计今年年底基本落实到各个市区和街道 ; 请大家积极配合政府相关部门工作', '<p><br></p><p><br></p><p>六部门印发关于进一步优化企业开办服务的通知</p><p>&nbsp;2020-08-21 10:51:17 157</p><p><br></p><p><br></p><p>近日，市场监管总局、国家发展改革委、公安部、人力资源社会保障部、住房和城乡建设部、税务总局六部门就进一步优化企业开办服务、做到企业开办全程网上办理有关事项印发通知。</p><p><br></p><p><br></p><p>主要内容如下：</p><p><br></p><p>切实做到企业开办全程网上办理</p><p><br></p><p><br></p><p>全面推广企业开办一网通办。</p><p><br></p><p>2020年年底前，各省（区、市）和新疆生产建设兵团全部开通企业开办一网通办平台（以下简称一网通办平台），在全国各地均可实现企业开办全程网上办理。</p><p><br></p><p>进一步深化线上线下融合服务。</p><p><br></p><p>依托一网通办平台，推行企业登记、公章刻制、申领发票和税控设备、员工参保登记、住房公积金企业缴存登记可在线上“一表填报”申请办理；具备条件的地方实现办齐的材料线下“一个窗口”一次领取，或者通过推行寄递、自助打印等实现“不见面”办理。</p><p><br></p><p>不断优化一网通办服务能力。</p><p><br></p><p>完善一网通办平台功能设计，加强部门信息共享，2020年年底前具备公章刻制网上服务在线缴费能力。推动实现相关申请人一次身份验证后，即可一网通办企业开办全部事项。鼓励具备条件的地方，实现企业在设立登记完成后仍可随时通过一网通办平台办理员工参保登记、住房公积金企业缴存登记等企业开办服务事项。</p><p><br></p><p>进一步压减企业开办时间、环节和成本</p><p><br></p><p><br></p><p>进一步压缩开办时间。</p><p><br></p><p>2020年年底前，全国实现压缩企业开办时间至4个工作日以内；鼓励具备条件的地方，在确保工作质量前提下，压缩企业开办时间至更少。</p><p><br></p><p>进一步简化开办环节。</p><p><br></p><p>2020年年底前，推动员工参保登记、住房公积金企业缴存登记通过一网通办平台，一表填报、合并申请，填报信息实时共享，及时完成登记备案。企业通过一网通办平台申请刻制公章，不再要求企业提供营业执照复印件以及法定代表人（负责人等）的身份证明材料。</p><p><br></p><p>进一步降低开办成本。</p><p><br></p><p>鼓励具备条件的地方，改变税控设备“先买后抵”的领用方式，免费向新开办企业发放税务Ukey。</p><p><br></p><p>大力推进电子营业执照、电子发票、电子印章应用</p><p><br></p><p><br></p><p>推广电子营业执照应用。</p><p><br></p><p>在加强监管、保障安全前提下，依托全国一体化政务服务平台，推广电子营业执照应用，作为企业在网上办理企业登记、公章刻制、涉税服务、社保登记、银行开户等业务的合法有效身份证明和电子签名手段。</p><p><br></p><p>推进电子发票应用。</p><p><br></p><p>继续推行增值税电子普通发票，积极推进增值税专用发票电子化。</p><p><br></p><p>推动电子印章应用。</p><p><br></p><p>鼓励具备条件的地方，出台管理规定，明确部门职责，细化管理要求，探索统筹推进电子印章应用管理，形成可复制推广的经验做法。</p>', '原创', '李记者', 1, 5, 0, '2020-11-27 15:45:17', NULL);
INSERT INTO `xn_news` VALUES (3, '西昌时代广场物业项目', '/uploads/20201202/8b8c6ec22d18c66eee3836dd715edf8e.jpg', 1, 6, '西昌时代广场物业项目 西昌时代广场项目是西昌首例集都市精英华宅、高端购物中心、国际级酒店于一体的新派城市综合体', '<h3 class=\"c_titile\" style=\"text-align: center;\"><br></h3><article><div class=\"infos\"><p class=\"MsoNormal\" align=\"left\">西昌时代广场项目是西昌首例集都市精英华宅、高端购物中心、国际级酒店于一体的新派城市综合体，是四川宏飞投资开发有限公司依托国际化视野，利用尊华集团的强大资源平台，取经万达广场、世豪广场等第五代商业模式，再结合西昌本土的传统文化和生活习惯，打造西昌最先进的城市综合体项目。</p><p class=\"MsoNormal\" align=\"left\"><span>时代广场总建筑面积7.3万方，住宅1.8万方，商业4万方，楼下-1F~4F是项目自带的集中商业（3万方），旁边还有1万方的高端酒店，在两主干道交汇的位置是下沉式广场，与负一楼无缝连接，并设有扶梯可直达地面商业，负一层到四层的商业开创了攀西地区首家名品购物中心，国际快时尚潮流中心、儿童主题生活馆及豪华星级影院、特色美食生活广场，旁边就是国际知名的酒店，holiday酒店(洲际旗下高端连锁酒店，洲际假日酒店)，集吃喝玩乐购物于一体。</span></p><p class=\"MsoNormal\" align=\"left\"><span>时代广场项目地处成南板块，位于航天大道与胜利南路交汇处，是西昌地标性建筑。而航天大道，是目前西昌唯一的一条双向八车道主干线，与海河板块（旅游商业为主），月城板块（西昌老城区）紧密相连，也是通往建昌板块（西昌新城区）最快速便捷的通道，西昌地标性的建筑都汇聚在这条主干道上。便捷的交通加上完善的配套，时代广场未来的升值保值空间是不可限量的。</span></p><p style=\"text-align: center;\"><img src=\"http://www.cdhhsy.com.cn/Public/extend/kindeditor/attached/image/20180301/20180301093900_14691.jpg\" alt=\"\" class=\"img-responsive center-block\"></p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\"><img src=\"http://www.cdhhsy.com.cn/Public/extend/kindeditor/attached/image/20180301/20180301093916_53375.jpg\" alt=\"\" class=\"img-responsive center-block\"></p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\"><img src=\"http://www.cdhhsy.com.cn/Public/extend/kindeditor/attached/image/20180301/20180301093942_39489.jpg\" alt=\"\" class=\"img-responsive center-block\"></p></div></article>', '原创', '李建明', 0, 3, 1, '2020-12-02 14:15:16', NULL);
INSERT INTO `xn_news` VALUES (2, '人民日报：武汉雷神山医院物业团队', '/uploads/20201127/d6fb0f10c7201ac82e3f64be67d2fade.jpg', 1, 1, '在武汉雷神山医院，因为有了他们，日常的保洁消毒、治安巡逻、配送分发盒饭等工作才得以有序运转。他们，便是共有400余人的雷神山医院物业团队。', '<h3 class=\"c_titile\" style=\"text-align: center;\">&nbsp;</h3><article><div class=\"infos\"><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span>在武汉雷神山医院，因为有了他们，日常的保洁消毒、治安巡逻、配送分发盒饭等工作才得以有序运转。</span><b><span class=\"15\"><span>他们，便是共有</span>400余人的雷神山医院物业团队。</span></b><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><b><span class=\"15\"><br></span></b></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span>2月16日，作为有着11年临床护理经验的武汉地产集团物业公司业务骨干杨玲得知雷神山医院ICU病房正式启用后，主动请缨：“我有经验，ICU病房我不进谁进！!”</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><span>在她的带领下，物业团队</span>4名成员承担起2间ICU病房和护士站等区域的保洁工作，每天工作10小时以上。除了正常保洁，杨玲还要负责1间ICU病房里仪器设备、病床的消毒杀菌工作。“每个患者病床旁都有一个1.2米高的大号垃圾桶，平均一天要清运30多袋医疗垃圾。”杨玲说。</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><span>疫情发生前，</span>&nbsp;<span>陈伟伟是一名专职网约车司机。</span>1月27日晚上11点，看到朋友圈里招募雷神山医院物业人员的消息，他打了一个咨询电话，两天之后就报上了名。“就如当初选择当兵一样，我想做点有意义的事情。”自2月2日进驻雷神山医院，陈伟伟和30名组员便负责32个病区的医疗垃圾转运工作。</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><span>刚到雷神山医院，陈伟伟和同伴每天要负责</span>100袋垃圾的清运。随着收治患者人数增加，这个数字不断攀升，600袋、1000袋……任务最重的一天，陈伟伟凌晨3点半才下班，回宿舍只睡了3个多小时又继续工作。</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span>“陈伟伟快来收垃圾，A16病区垃圾堆满了。”微信工作群里的消息几乎没断过。作为组长，不管谁呼叫，陈伟伟都第一时间回复并赶到。“谁都怕病毒，但不能因为怕就不处理，我愿意为抗击疫情出一份力。”</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><span>雷神山医院的物业管家、党员李炫汐已经坚守岗位一个多月。</span>1993年出生的李炫汐，负责雷神山医院的设备维修、生活物资采购等工作。刚开始，医院2天之内有12个病区开放，各病区护士长对物业工作的要求不尽相同。李炫汐按照各病区的要求协调跟进，每天都要接120多个电话。</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><br></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><span><span>为了制定精准的物业工作流程，李炫汐一次又一次进入隔离区病房和垃圾转运队，测算每个人的任务量。李炫汐说，</span>“危险的确是有的，可武汉养育了我，这个时候我要回报这个英雄的城市。”</span><span></span></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: justify;\"><br></p><p class=\"MsoNormal\" align=\"justify\" style=\"text-align: right;\"><span>转载出处：中国物业管理协会</span></p></div></article>', '中国物业管理协会', '管理员', 1, 1, 0, '2020-11-27 17:07:03', NULL);
INSERT INTO `xn_news` VALUES (4, '成都办公区物业项目', '/uploads/20201202/d4ee418bb6c96af9897fbeb0efb30da4.jpg', 1, 6, '成都办公区项目部实行项目经理负责制，是成都金浣花实业有限公司下设的一个生产项目部，专门从事物业管理', '<h3 class=\"c_titile\" style=\"text-align: center;\"><br></h3><article><div class=\"infos\"><p class=\"MsoNormal\" align=\"left\"><span>成都办公区项目部实行项目经理负责制，是成都金浣花实业有限公司下设的一个生产项目部，专门从事物业管理服务，下设综合服务中心和设备管理中心两大专业中心。</span></p><p class=\"MsoNormal\"><span>目前，项目部在册员工</span><span>88</span><span>人，其中：管理人员</span><span>8</span><span>人、客服、会务人员</span><span>10</span><span>人、工程技术人员</span><span>14</span><span>人、保洁人员和保安人员以劳务外包形式为管理项目提供服务。本科生以上学历的员工占</span><span>5.62%</span><span>、大专生学历的职工占</span><span>22.47%</span><span>、其他学历的职工占</span><span>71.91%</span><span>；项目经理持有注册物业管理师资格证书，技术人员持有专业技术职称证书。</span><span></span></p><p class=\"MsoNormal\"><span>项目部现服务的项目 “成都办公区”</span><span>2017</span><span>年被评定为“成都市物业管理优秀项目”，位于浣花北路</span><span>1</span><span>号，为现代化高级智能写字楼，单一业主为中国电建集团成都勘测设计研究院有限公司，大楼全部用于业主办工之用。大楼主体工程于</span><span>1999</span><span>年</span><span>12</span><span>月</span><span>18</span><span>日开工，</span><span>2001</span><span>年底竣工，</span><span>2002</span><span>年</span><span>5</span><span>月正式投入使用，总建筑面积</span><span>28494.78</span><span>平方米。</span></p><p class=\"MsoNormal\"><span>成都办公区内外装饰规范气派，设备先进、功能齐全、管理复杂、技术要求高。大楼的弱电系统为业主提供了先进的网络设备和进口的语音对讲系统，设置了足够的网络接口，保证各设计专业</span><span>2000</span><span>多名员工人手一台电脑、一部电话；良好的闭路电视监控设施，先进的烟感、温感、喷淋消防联动智能装置，消防增压设备全为自控装置，与大楼消防系统联动，完备的应急照明系统和引路标志，构筑了完善的消防和安保系统；大楼</span><span>A</span><span>座</span><span>4</span><span>部通力电梯（</span><span>18</span><span>层站</span><span>3</span><span>台、</span><span>9</span><span>层站</span><span>1</span><span>台），</span><span>C</span><span>座</span><span>1</span><span>部通力电梯（</span><span>8</span><span>层站），形成了大楼安全快捷的交通通道；大楼二次供水系统管道全部用于卫生间及消防用水，排水系统采用</span><span>PVC</span><span>管，不受腐蚀，噪音降低，形成了大楼安全可靠的给排水系统；</span><span>2</span><span>台进口特灵中央空调制冷机组，</span><span>2</span><span>台菲斯曼制热机组，所有管道系统均用保温材料包裹并用不锈钢饰面，配有远程控制的通讯系统，为大楼提供舒适的温湿度环境和通讯及时性创造了条件；大楼管理上采用先进的电子门禁系统，对所有上班人员实施打卡管理；大楼办公供电为双电源。本建筑区规划建设机动车停车位</span><span>217</span><span>个，出入口</span><span>2</span><span>个。大楼</span><span>B</span><span>座中门位置设有残疾人轮椅进出的坡道，为残疾人的进出大楼提供便利。</span></p><p class=\"MsoNormal\"><span></span></p><p class=\"MsoNormal\"><span>项目部组建以来，以科学发展观为指导，紧紧围绕“以服务求生存、以管理求提升、以创新求发展”的战略思想，本着“细节成就品质，服务创造价值”的服务理念，推行“一站式”以全体业主获得持续满意为宗旨，不断转变观念，创新工作思路，强化内部管理，加强员工培训，提升整体管理水平和服务质量。</span></p></div></article>', '原创', '李建明', 0, 3, 1, '2020-12-02 14:25:56', NULL);
INSERT INTO `xn_news` VALUES (5, '物业服务', '/uploads/20201203/c8f12dd861fd58e7d4d9b358b76a7f37.jpg', 1, 7, 'PROPERTY SERVICE', '<p class=\"MsoNormal\"><span><span><span>公司物业服务</span></span></span><span>领域</span><span>涵盖酒店物业、学校物业、办公物业、工业物业、商业物业、工程物业及住宅物业，总体服务面积达128</span><span>万平米。</span></p><p class=\"MsoNormal\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; 秉持“五心”级服务理念</strong></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;安全放心、专业贴心、健康舒心、独具匠心、共建齐心</span></p><p class=\"MsoNormal\"><span><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; 努力打造十大特色服务</strong></span></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;1</span><span>、“亲情化”物业服务&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span>6</span><span>、“零干扰”服务 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;2</span><span>、 保安上岗“无手机”&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><span>7</span><span>、项目“值班经理制度”</span></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;3</span><span>、 写字楼商务服务&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span>8</span><span>、“轮流例会制度”</span></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;4</span><span>、 网格化客户经理制度&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span>9</span><span>、垃圾滞留时间标准化</span></p><p class=\"MsoNormal\"><span>&nbsp;&nbsp;&nbsp;&nbsp;5</span><span>、 物业管理“首问责任制” &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span>10</span><span>、智能化、数字化楼宇管理</span></p><p class=\"MsoNormal\" style=\"text-align: center;\"><span><img src=\"http://www.cdhhsy.com.cn/Public/extend/kindeditor/attached/image/20180301/20180301031009_57629.jpg\" alt=\"\" class=\"img-responsive center-block\"></span></p><div><span><br></span></div>', '原创', '李建明', 0, 2, 1, '2020-12-03 09:21:47', NULL);
INSERT INTO `xn_news` VALUES (6, '商旅服务', '/uploads/20201203/e9b7fa59d12aaaa0744e0f8cf1d34734.jpg', 1, 7, 'ELERONIC COMMERCE', '<p><span>&nbsp;公司全资子公司-中电建商旅旅行社成都有限公司致力于为客户提供优质、便捷、高效的商旅出行服务，目前已为包括电建集团、川投集团等数十万客户提供商旅出行服务。</span></p><p><span>&nbsp; &nbsp; &nbsp; 中电建商旅旅行社成都有限公司与各大航空公司开展友好合作。2019年先后荣获中国南方航空股份有限公司四川分公司优秀国内销售合作伙伴、中国国际航空公司最佳国内品质奖荣誉称号。<br></span></p><p><span></span><span></span><span></span><span></span><span></span></p><p style=\"text-align: center;\"><img src=\"http://www.cdhhsy.com.cn/Public/extend/kindeditor/attached/image/20180301/20180301030752_23635.jpg\" alt=\"\" class=\"img-responsive center-block\"></p>', '原创', '李建明', 0, 6, 1, '2020-12-03 10:00:56', NULL);

-- ----------------------------
-- Table structure for xn_sell
-- ----------------------------
DROP TABLE IF EXISTS `xn_sell`;
CREATE TABLE `xn_sell`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '售票设置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_sell
-- ----------------------------
INSERT INTO `xn_sell` VALUES (1, '售票数量', 1, 'v', '2022-01-18 10:26:11', NULL);

-- ----------------------------
-- Table structure for xn_upload_files
-- ----------------------------
DROP TABLE IF EXISTS `xn_upload_files`;
CREATE TABLE `xn_upload_files`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage` tinyint(1) NULL DEFAULT 0 COMMENT '存储位置 0本地',
  `app` smallint(4) NULL DEFAULT 0 COMMENT '来自应用 0前台 1后台',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '根据app类型判断用户类型',
  `file_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `file_size` int(11) NULL DEFAULT 0,
  `extension` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '文件后缀',
  `url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '图片路径',
  `create_time` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 380 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xn_upload_files
-- ----------------------------
INSERT INTO `xn_upload_files` VALUES (335, 0, 1, 1, 'logo2.png', 9451, 'png', '/uploads/20200318/137b548c5ece2e8efc3dfa54c0b3f7e1.png', 1584527929);
INSERT INTO `xn_upload_files` VALUES (336, 0, 1, 1, '475bb144jw8f9nwebnuhkj20v90vbwh9.jpg', 7301, 'jpg', '/uploads/20200318/39f3d8289e1cc2043af5d244893af728.jpg', 1584542682);
INSERT INTO `xn_upload_files` VALUES (337, 0, 1, 1, '961a9be5jw8fczq7q98i7j20kv0lcwfn.jpg', 6188, 'jpg', '/uploads/20200318/530df18f012084a77488c8f983eb6a04.jpg', 1584546128);
INSERT INTO `xn_upload_files` VALUES (338, 0, 1, 1, '11.jpg', 7798, 'jpg', '/uploads/20200318/dc390824c6b38a8134af7b4dce6205ea.jpg', 1584546758);
INSERT INTO `xn_upload_files` VALUES (348, 0, 1, 1, '1.png', 279041, 'png', '/uploads/20201124/33f8e3d7745df6c40f794dd7c16244b5.png', 1606183416);
INSERT INTO `xn_upload_files` VALUES (349, 0, 1, 1, '2.png', 321814, 'png', '/uploads/20201124/b76c0119c8263e04ad523c91e2c6fc0d.png', 1606183435);
INSERT INTO `xn_upload_files` VALUES (350, 0, 1, 1, '3.png', 352973, 'png', '/uploads/20201124/62ab70b7b0387b4c4f1362c90aaf4e47.png', 1606183435);
INSERT INTO `xn_upload_files` VALUES (351, 0, 1, 1, '4.png', 524288, 'png', '/uploads/20201124/6793c30397d21cec7392b075fe3f1a36.png', 1606183487);
INSERT INTO `xn_upload_files` VALUES (353, 0, 1, 1, '5.png', 260580, 'png', '/uploads/20201124/cc9431cc846afcaa3bda85084dde60e8.png', 1606183590);
INSERT INTO `xn_upload_files` VALUES (354, 0, 1, 1, '7.png', 309031, 'png', '/uploads/20201124/05f091af93459c1cfc190a886c4bff1f.png', 1606183772);
INSERT INTO `xn_upload_files` VALUES (363, 0, 1, 1, '1.png', 279041, 'png', '/uploads/20201124/88229d76f367985cef45cb954b12dc82.png', 1606184748);
INSERT INTO `xn_upload_files` VALUES (356, 0, 1, 1, 'gutenberg-editing-blocks.gif', 524288, 'gif', '/uploads/20201124/b6c2ff4df298df924794de7d35ce926b.gif', 1606184365);
INSERT INTO `xn_upload_files` VALUES (347, 0, 1, 1, 'QQ20201123133658.jpg', 16955, 'jpg', '/uploads/20201124/e4effe9dc6c899b40bbd9388bbdf9109.jpg', 1606182432);
INSERT INTO `xn_upload_files` VALUES (364, 0, 1, 1, '2.png', 321814, 'png', '/uploads/20201124/b9d3d899759f69dc70e32471eadb7b0e.png', 1606184748);
INSERT INTO `xn_upload_files` VALUES (362, 0, 1, 1, '1.png', 279041, 'png', '/uploads/20201124/2d7dbbf06c68a8e800c16e3f091bfa19.png', 1606184439);
INSERT INTO `xn_upload_files` VALUES (365, 0, 1, 1, 'QQ20201123133658.jpg', 16955, 'jpg', '/uploads/20201124/8a387ef26e05db023843cecd6559403e.jpg', 1606184748);
INSERT INTO `xn_upload_files` VALUES (366, 0, 1, 1, 'news_banner.jpg', 136962, 'jpg', '/uploads/20201126/11cc4a6ad37ffdc07be45cf12f31890d.jpg', 1606382003);
INSERT INTO `xn_upload_files` VALUES (367, 0, 1, 1, '5a55e6e45e144.jpg', 315393, 'jpg', '/uploads/20201127/d6fb0f10c7201ac82e3f64be67d2fade.jpg', 1606448008);
INSERT INTO `xn_upload_files` VALUES (368, 0, 1, 1, '5a55e6f348a2a.jpg', 328124, 'jpg', '/uploads/20201127/5cf71d33e336431ee835ec4d79ca0fe2.jpg', 1606448008);
INSERT INTO `xn_upload_files` VALUES (369, 0, 1, 1, '5a55e6ffa8367.jpg', 355180, 'jpg', '/uploads/20201127/5e13100db5e005081b98bc683639b22c.jpg', 1606448008);
INSERT INTO `xn_upload_files` VALUES (370, 0, 1, 1, '0f77b1da989f46549f9ca2b7b036f805.jpg', 30614, 'jpg', '/uploads/20201127/4949d5c677b1dbdc228c716bea761848.jpg', 1606464604);
INSERT INTO `xn_upload_files` VALUES (371, 0, 1, 1, 'news_banner.jpg', 136962, 'jpg', '/uploads/20201130/2f1d3f4853ec1e4721f262e38f844605.jpg', 1606724783);
INSERT INTO `xn_upload_files` VALUES (372, 0, 1, 1, '5a4f2a5b9559e.jpg', 323511, 'jpg', '/uploads/20201202/8b8c6ec22d18c66eee3836dd715edf8e.jpg', 1606889745);
INSERT INTO `xn_upload_files` VALUES (373, 0, 1, 1, '5a9769ecd5651.jpg', 279556, 'jpg', '/uploads/20201202/d4ee418bb6c96af9897fbeb0efb30da4.jpg', 1606889746);
INSERT INTO `xn_upload_files` VALUES (374, 0, 1, 1, '5a5311607b7b9.jpg', 32241, 'jpg', '/uploads/20201203/c8f12dd861fd58e7d4d9b358b76a7f37.jpg', 1606958553);
INSERT INTO `xn_upload_files` VALUES (375, 0, 1, 1, '5a44a6c6cdd42.jpg', 30235, 'jpg', '/uploads/20201203/e9b7fa59d12aaaa0744e0f8cf1d34734.jpg', 1606960800);
INSERT INTO `xn_upload_files` VALUES (376, 0, 1, 1, '3.png', 352973, 'png', '/uploads/20210311/3410367dc59b514a9236be3b19228d96.png', 1615447405);
INSERT INTO `xn_upload_files` VALUES (377, 0, 1, 1, '4.png', 524288, 'png', '/uploads/20210311/f56663682de4dc3cda70d8ab9f1251e7.png', 1615447467);
INSERT INTO `xn_upload_files` VALUES (378, 0, 1, 1, '4.png', 15554, 'png', '/uploads/20210311/94eb749af4501d4dee0d415ae92c21a7.png', 1615447467);
INSERT INTO `xn_upload_files` VALUES (379, 0, 1, 1, 'logo.png', 46035, 'png', '/uploads/20220113/5ea3ced664b085607e21e0e5a76f7e8b.png', 1642046030);

SET FOREIGN_KEY_CHECKS = 1;
